from django.db import models

class Cart(models.Model):
    
    user_id = models.IntegerField(null=True)
    product_id = models.IntegerField(null=True)
    quantity = models.IntegerField(null=True)
    sessions_id = models.CharField(max_length=250, null=True)
    subscription = models.IntegerField(null=True)
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.IntegerField(null=True)
    
    class Meta:
        db_table="sih_cart"

class Subscription(models.Model):
    product_id = models.IntegerField(null=True)
    subscription_month = models.IntegerField(null=True)
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.IntegerField(null=True)

    class Meta:
        db_table="sih_subscription"
        