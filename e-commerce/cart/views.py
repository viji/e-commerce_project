from django.shortcuts import render, redirect  
from datetime import date, timedelta
from django.http import HttpResponseRedirect 
from django.http import HttpResponse
from django.conf import settings
from django.utils.crypto import get_random_string
import os.path
import json
from django.http import JsonResponse

from datetime import date, timedelta
import datetime

from product.models import Product
from product.models import ProductPackageType
from product_group.models import ProductGroup
from cart.models import Cart
from cart.models import Subscription
from coupon.models import Coupon
from user.models import Users
from user.models import UsersType
from user.models import UserCodes
from user.models import UserProfile
from address.models import Address
from django.contrib import messages
def load_cart(request):
	cart_info=where_str=sql=""
	if "loggedin_user_id" in request.session:
		user_id=request.session['loggedin_user_id']
		# cart_info = Cart.objects.filter(user_id=user_id)
		where_str=" AND cart.user_id = "+str(user_id)
	else:
		sessions_id = request.COOKIES['csrftoken']
		# cart_info = Cart.objects.filter(sessions_id=sessions_id)
		where_str=" AND cart.sessions_id = "+repr(sessions_id)
	if where_str:
		sql = "SELECT cart.*,product.name,product.product_code,product.offer_price,product.actual_price,product.pack_id,product.weight FROM sih_cart as cart,sih_product as product WHERE cart.product_id = product.id"+where_str
	# sql = "SELECT cart.*,product.name,product.product_code,product.offer_price,product.actual_price FROM sih_cart as cart,sih_product as product WHERE cart.product_id = product.id"
	if sql:
		cart_info = Cart.objects.raw(sql)
	#print(cart_info.query)	
	image = "images/product-3.jpg"

	temp_cart_array={}
	cart_array={}

	sub_total=0
	total=0
	subscription_val=0
	if cart_info:
		authoinc=1
		for value in cart_info:
			print(type(value.subscription))
			if value.offer_price and value.subscription!=0:
				total=value.quantity * value.offer_price * value.subscription
				offer_price = value.offer_price
			elif value.offer_price:
				total=value.quantity * value.offer_price
				offer_price = value.offer_price
			elif value.subscription!=0:
				offer_price=0
				tot_val=value.quantity * value.subscription
				print(tot_val)
				total=tot_val * value.actual_price
				print(total)
			else:
				offer_price=0
				print(value.actual_price)
				total=value.quantity * value.actual_price
				print(total)
			cart_array[authoinc]={
				"id":value.id,
				"product_id":value.product_id,
				"name":value.name,
				"pack_id":value.pack_id,
				"qty":value.quantity,
				"subscription":value.subscription,
				"weight":value.weight,
				"offer_price":offer_price,
				"actual_price":value.actual_price,
				"total":total,
				"image":image,
			}
			sub_total = sub_total+total
			authoinc=authoinc+1

	coupon_code=""
	coupon_info=""
	coupon_error=""
	discount_amt = 0
	discount_amt = 0
	over_all_total = 0
	if request.POST.get('remove_coupon_code'):
		if 'cart_coupon_code' in request.session:
			del request.session['cart_coupon_code']
			
	if request.POST.get('coupon_code'):
		
		if 'cart_coupon_code' in request.session:
			del request.session['cart_coupon_code']
			
		coupon_code = request.POST.get('coupon_code').strip()
		if coupon_code != "":
			try:
				coupon_info=Coupon.objects.get(title=coupon_code)
			except:
				coupon_info=""
		
			
			if coupon_info:
				
				today = date.today()
				
				if coupon_info.end_date > today:
					if coupon_info.customer_frequency != 0:
						
						if coupon_info.min_order_amount <= sub_total:
							request.session["cart_coupon_code"] = coupon_code
							discount_amt = coupon_info.offer_percentage*sub_total/100
						else:
							coupon_error="Order amount less than of minimum order amount"
					else:
						coupon_error="Coupon code user limit over"
				else:
					coupon_error="Coupon code expiered"
			else:
				coupon_error="Invalid coupon"    
		else:
			coupon_error="Please enter coupon"    

	over_all_total = sub_total-discount_amt

	delivery_charge=0
	amount_info={
				'sub_total':sub_total,
				'delivery_charge':delivery_charge,
				'discount_amt':discount_amt,
				'over_all_total':over_all_total,
	}
	request.session["cart_amount_info"] = amount_info
	request.session["cart_product_info"] = cart_array
	#request.session["cart_product_subscription"] = cart_array.subscription

	return render(request,"cart.html",{'cart_array':cart_array,'amount_info':amount_info,'coupon_error':coupon_error,'coupon_code':coupon_code})
	
def get_user_data(request):
	response_data={}
	email=""
	password=common_error=""
	user_flag=0
	redirect=""
	sessions_id = request.COOKIES['csrftoken']
	if request.method == "POST":
		if request.POST.get('email'):
			email = request.POST.get('email').strip()
		if request.POST.get('password'):
			password = request.POST.get('password').strip()
	if email:
		
		if Users.objects.filter(email=email).exists():
			user_flag=1
			
			if email and password:
				
				try:
					user_info = Users.objects.get(email=email,password=password)
				except:
					user_info=""
				
				if user_info:
					
					user_type = UsersType.objects.get(id=user_info.user_type_id)
					user_profile_info = UserProfile.objects.get(user_id=user_info.id)
					
					request.session['loggedin_user_id']=user_info.id
					request.session['loggedin_user_code']=user_info.user_code
					request.session['loggedin_user_type']=user_type.code
					request.session['loggedin_user_email']=user_info.email
					request.session['loggedin_user_name']=user_profile_info.first_name+" "+user_profile_info.last_name
					
					Cart.objects.filter(sessions_id=sessions_id).update(user_id=request.session['loggedin_user_id'],sessions_id="")
					redirect="/confirm_address/0"
					
				else:
					common_error="Invalid password"
					
		else:
		
			user_id_val=UserCodes(id=None)
			user_id_val.save()
			id_last = UserCodes.objects.latest('id')
			user_code="USR000"+str(id_last.id)
			password = get_random_string(length=8)
			users_values=Users(user_code=user_code,user_type_id=2,email=email,password=password,status=1)
			users_values.save()
			user_id_last = Users.objects.latest('id')
			user_profile_values=UserProfile(first_name="User",last_name="",user_id=user_id_last.id)
			user_profile_values.save()
		
			try:
				user_info = Users.objects.get(email=email,password=password)
			except:
				user_info=""
			
			if user_info:
				
				user_type = UsersType.objects.get(id=user_info.user_type_id)
				user_profile_info = UserProfile.objects.get(user_id=user_info.id)
				
				request.session['loggedin_user_id']=user_info.id
				request.session['loggedin_user_code']=user_info.user_code
				request.session['loggedin_user_type']=user_type.code
				request.session['loggedin_user_email']=user_info.email
				request.session['loggedin_user_name']=user_profile_info.first_name+" "+user_profile_info.last_name
				Cart.objects.filter(sessions_id=sessions_id).update(user_id=request.session['loggedin_user_id'],sessions_id="")
				redirect="/confirm_address/0"
			
	response_data['user_flag'] = user_flag		
	response_data['common_error'] = common_error	
	response_data['redirect'] = redirect	
	
	return JsonResponse(response_data) 
	
	
def confirm_address(request,address_id):

	loginb_flag=0
	user_address=default_address=""
	if "loggedin_user_id" in request.session:
		if request.session['loggedin_user_id']:
			user_address=Address.objects.filter(created_by=request.session['loggedin_user_id'],status=1)
			if address_id == 0:
				try:
					default_address = Address.objects.get(created_by=request.session['loggedin_user_id'],status=1,default=1)
				except:
					default_address =""
			else:
				try:
					default_address = Address.objects.get(id=address_id)
				except:
					default_address =""
		else:
			return HttpResponseRedirect("/cart")		
	else:
		return HttpResponseRedirect("/cart")
	
	return render(request,"billing_shipping_address.html",{'default_address':default_address,'user_address':user_address})

def item_update(request):
    if request.method == 'POST':
        if "update_qty" in request.POST:  
            product_id = request.POST.get('product_id','')
            print(product_id)
            quantity = request.POST.get('quantity','')
            cart_item = Cart.objects.filter(id=product_id).update(quantity=quantity)
            messages.success(request, 'Item Quantity updated successfully.')
    return HttpResponseRedirect("/cart")

def item_remove(request, id):  
    item = Cart.objects.get(id=id)  
    item.delete() 
    messages.success(request, 'Item removed successfully.')
    return HttpResponseRedirect("/cart")

def subscription_update(request):
	if request.method == 'POST':
		if "update_subs" in request.POST:  
			product_id = request.POST.get('product_id','')
			print(product_id)
			print("kdjkjd")
			subscription = request.POST.get('subscription','')
			print(subscription)
			cart_item = Cart.objects.filter(product_id=product_id).update(subscription=subscription)
			subs_item=Subscription.objects.filter(product_id=product_id).update(subscription_month=subscription,modified_on= datetime.datetime.utcnow())
			messages.success(request, 'Item subscription updated successfully.')
	return HttpResponseRedirect("/cart")