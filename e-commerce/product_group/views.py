from django.shortcuts import render, redirect  
from datetime import date, timedelta
from django.http import HttpResponseRedirect 
from django.http import HttpResponse
from django.conf import settings
import os.path
from common.views import admin_required
from category.models import Category
from product_group.forms import ProductGroupForm
from product_group.forms import UploadsForm
from product_group.forms import ProductGroupEditForm
from product_group.models import ProductGroup
from product_group.models import ProductGroupCodes
from product_group.models import ProductGroupImage
import datetime
from django.contrib import messages
from user.models import UserProfile
from django.db.models.functions import Concat
import math
from django.core.files.storage import FileSystemStorage

@admin_required
def product_group_add(request):
	cat_list=Category.objects.all().exclude(status=2)
	forms=""
	if request.method=='POST':
		forms = ProductGroupForm(request.POST)
		print(forms.errors)
		if forms.is_valid():
			if request.POST.get('cat_id'):
			    cat_id = request.POST.get('cat_id')
			if request.POST.get('prt_name'):
			    prt_name = request.POST.get('prt_name').strip()
			if request.POST.get('prt_status'):
			    prt_status = request.POST.get('prt_status')
			prt_id_val=ProductGroupCodes(id=None)
			prt_id_val.save()
			print(prt_id_val)
			prt_id_last = ProductGroupCodes.objects.latest('id')
			prt_code="PRTGRP000"+str(prt_id_last.id)
			created_on = datetime.datetime.utcnow()
			created_by = request.session['loggedin_user_id']
			modified_on = None
			modified_by = None
			prt_values=ProductGroup(prd_group_code=prt_code,category_id=cat_id,name=prt_name,status=prt_status,created_on=created_on,created_by=request.session['loggedin_user_id'],modified_on=modified_on,modified_by=modified_by)
			prt_values.save()
			messages.success(request, 'ProductGroup added successfully.')
			return redirect('/product_group_list')
	else:
		forms = ProductGroupForm()
	return render(request,"product_group_add.html",{'cat_list':cat_list,'forms':forms})
    
@admin_required
def product_group_edit(request,id):
	forms=prt_status=""
	prt_values=ProductGroup.objects.get(id=id)
	product_values=ProductGroup.objects.all()
	if request.method=='POST':
		forms = ProductGroupEditForm(request.POST)
		print(forms.errors)
		if forms.is_valid():
			if request.POST.get('prt_name'):
			    prt_name = request.POST.get('prt_name').strip()
			if request.POST.get('prt_description'):
			    prt_des = request.POST.get('prt_description')
			if request.POST.get('prt_status'):
			    prt_status = request.POST.get('prt_status')
			modified_on = datetime.datetime.utcnow()
			prt_values=ProductGroup.objects.filter(id=id).update(name=prt_name,description=prt_des,status=prt_status,modified_on=modified_on,modified_by=request.session['loggedin_user_id'])
			messages.success(request, 'ProductGroup Updated successfully.')
			return redirect('/product_group_list')
	return render(request,"product_group_edit.html",{'forms':forms,'prt_values':prt_values,'product_values':product_values,'product_id':id})    

@admin_required    
def product_group_list(request):
	cat_values=Category.objects.all()
	user_pro_details=UserProfile.objects.all()
	sql = where_str =""
	query_data={}
	search_keyword=""
	prtgrp_status=""
	search_cat_code=""

	#filter data set start
	created_by=request.session['loggedin_user_id']
	print(created_by)
	query_data["created_by"] = created_by
	if request.POST.get('search_keyword',''):
		search_keyword=request.POST.get('search_keyword','')
		query_data['keyword__contains'] = search_keyword
	if request.POST.get('search_cat_code',''):
		search_cat_code=request.POST.get('search_cat_code','')
		query_data['category_id'] = search_cat_code
	if request.POST.get('status',''):
		prtgrp_status=request.POST.get('status','')
		query_data['status'] = prtgrp_status
	#filter data set end    

	# sorting starts
	sort_str=""
	varSortColumn="created_on"
	varSortOrderVal="DESC"
	if request.POST.get('varSortOrder'):
		varSortOrderVal = request.POST.get('varSortOrder')

	if request.POST.get('varSortCol'):
		varSortColumn = request.POST.get('varSortCol')

	if varSortOrderVal == "ASC":
		sort_str=varSortColumn
	elif varSortOrderVal == "DESC":
		sort_str='-'+varSortColumn

	#paginations starts

	varPageNo = settings.PAGE_NO
	rec_count = 5

	if request.POST.get('count'):
		rec_count = request.POST.get('count')

	if request.POST.get('varPageNo'):
		varPageNo = request.POST.get('varPageNo')

	pageNo = int(varPageNo) - int(1)

	start_from = int(pageNo) * int(rec_count)

	pagination = " LIMIT " + str(start_from) + "," + str(rec_count)

	page_cnt=ProductGroup.objects.all().annotate(keyword=Concat('name','prd_group_code')).filter(**query_data).order_by(sort_str)
	print(page_cnt)
	# print(page_cnt.query)
	rec_count_value=settings.REC_COUNT


	page_cnt = len(page_cnt)
	no = int(page_cnt)/int(rec_count)

	total_pages = math.ceil(no);
	startRec=1
	endRec=""
	if int(varPageNo) == 1:
		if int(page_cnt) < int(rec_count):
			endRec = page_cnt
		else:
			endRec = rec_count
	else:
		startRec = int(rec_count) * int(pageNo)
		endRec   = int(startRec) + int(rec_count)
	if int(endRec) > int(page_cnt):
		endRec = page_cnt

	startRec += 1

	#paginations ends
	search_val={
	'search_keyword':search_keyword,
	'search_prtgrp_code':search_cat_code,
	'search_prtgrp_status':prtgrp_status

	}
	productgroup_data = ProductGroup.objects.all().annotate(keyword=Concat('name','prd_group_code')).filter(**query_data).order_by(sort_str)[int(start_from):start_from+int(rec_count)]
	return render(request,"product_group_list.html",{'cat_values':cat_values,'user_pro_details':user_pro_details,'search_val':search_val,'endRec':endRec,'startRec':startRec,'total_pages':range(total_pages),'page_cnt':page_cnt,'varPageNo':int(varPageNo),'total_no':int(total_pages),'rec_count':int(rec_count),'rec_count_value':rec_count_value,'tatal_rec' : page_cnt,'varSortCol':varSortColumn,'varSortOrder':varSortOrderVal,'productgroup_data':productgroup_data})

@admin_required
def product_group_view(request,id):
	cat_values=Category.objects.all()
	user_pro_details=UserProfile.objects.all()
	product_group_values=ProductGroup.objects.get(id=id)
	context = {}
	image_error=""
	default=""
	img_query_list=[]
	if request.method == 'POST':
		form = UploadsForm(request.POST,request.FILES)
		
		if form.is_valid():
			
			for filename in request.FILES.getlist('image'):
				print(filename)
				uploaded_file = filename
				ext =os.path.splitext(uploaded_file.name)[1]
				valid_extensions = ['.jpg', '.png', '.jpeg']
				if not ext.lower() in valid_extensions:
					image_error="Doctor created successfully but image not upload."
				else:
					fs = FileSystemStorage()
					name = fs.save(filename.name, filename)
					context['url'] = fs.url(name)
					file_name_data = context['url'].replace("%20", " ")
				
					image_form=ProductGroupImage(prd_group_id=id,image=file_name_data,is_default=2)
					img_query_list.append(image_form)
					
			if img_query_list:
				ProductGroupImage.objects.bulk_create(img_query_list)
				messages.success(request, 'Product group image added successfully.')
				return redirect('/product_group_view/'+str(id))
	#print(img)
	images=ProductGroupImage.objects.filter(prd_group_id=id)

	return render(request,"product_group_view.html",{'cat_values':cat_values,'user_pro_details':user_pro_details,'product_group_values':product_group_values,"image_error":image_error,'images':images})   


@admin_required
def product_group_img_default(request,id):
	product_img=ProductGroupImage.objects.get(id=id)
	ProductGroupImage.objects.filter(id=id).update(is_default=1)
	ProductGroupImage.objects.filter(prd_group_id=product_img.prd_group_id).exclude(id=id).update(is_default=2)
	messages.success(request, 'Default image successfully Updated.')
	return redirect('/product_group_view/'+str(product_img.prd_group_id))

@admin_required
def product_group_delete(request, id):
	ProductGroup.objects.filter(id=id).delete()
	messages.success(request, 'Product Group Deleted successfully.')
	return HttpResponseRedirect("/product_group_list")