from django.db import models

class ProductGroupCodes(models.Model): 
    
    class Meta:
        db_table="sih_product_group_codes"
        
class ProductGroup(models.Model):
    
    prd_group_code    = models.CharField(max_length=250, null=True)
    category_id =models.IntegerField(null=True)
    name = models.CharField(max_length=250, null=True)
    description =models.TextField(null=True)
    status =models.SmallIntegerField(null=True)
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.IntegerField(null=True)
    
    class Meta:
        db_table="sih_product_group"


class ProductGroupImage(models.Model):
 
    prd_group_id =models.IntegerField(null=True)
    image =models.TextField(null=True)
    is_default =models.SmallIntegerField(null=True)
    
    class Meta:
        db_table="sih_product_group_image"