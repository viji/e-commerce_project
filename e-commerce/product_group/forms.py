from django import forms
from django.core.exceptions import ValidationError
from product_group.models import ProductGroup
from product_group.models import ProductGroupCodes
from product_group.models import ProductGroupImage
import re

class ProductGroupForm(forms.Form):

    cat_id = forms.CharField(error_messages={'required': 'Empty or invalid category Name'})
    prt_name = forms.CharField(error_messages={'required': 'Empty or invalid Product Name'})
    prt_description = forms.CharField(required=False)
    prt_status = forms.IntegerField(required=False)
    #cleaned_data = super().clean()
    def clean_prt_name(self):
        pn = super().clean() 
        prt_name = pn.get('prt_name')
        prt_val=ProductGroup.objects.filter(name=prt_name)
        if not prt_val:   
            return True;
        else:  
            msg = "Product Name already exists"
            self.add_error('prt_name', msg)

class ProductGroupEditForm(forms.Form):

    prt_id = forms.CharField(error_messages={'required': 'Empty or invalid category Name'})
    prt_name = forms.CharField(error_messages={'required': 'Empty or invalid Product Name'})
    prt_description = forms.CharField(required=False)
    prt_status = forms.IntegerField(required=False)
    product_id = forms.IntegerField(required=False)
    #cleaned_data = super().clean()
    def clean(self):
        pn = super().clean() 
        prt_name = pn.get('prt_name')
        product_id_val=pn.get('product_id')
        print(product_id_val)
        prt_val=ProductGroup.objects.filter(name=prt_name).exclude(id=product_id_val)
        if not prt_val:   
            return True;
        else:  
            msg = "Product Name already exists"
            self.add_error('prt_name', msg)

class UploadsForm(forms.Form):
    image  = forms.FileField()
    #product_id =forms.CharField(required=False)
    
    class Meta:  
        model = ProductGroupImage  
        #fields = "__all__"
        fields = ['image']