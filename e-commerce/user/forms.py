from django import forms
from django.core.exceptions import ValidationError
from user.models import UserProfile
from user.models import Users
import re

class UserForm(forms.Form):
    user_id = forms.IntegerField(error_messages={'required': 'Empty or invalid User Type'})
    first_name = forms.CharField(error_messages={'required': 'Empty or invalid First name'})
    last_name = forms.CharField(required=False)
    dob = forms.DateField(required=False)
    email = forms.CharField(error_messages={'required': 'Empty or invalid Email'})
    password1 = forms.CharField(error_messages={'required': 'Empty or invalid password'})
    password2 = forms.CharField(error_messages={'required': 'Empty or invalid password'})
    gender = forms.IntegerField(required=False)
    mobile_number = forms.CharField(required=False)
    user_status = forms.IntegerField(required=False)
    #cleaned_data = super().clean()
    
    def clean_email(self):
        cd = super().clean()
        email = cd.get('email')
        email_val=Users.objects.filter(email=email)
        if email_val:
            msg = "Email already exists"
            self.add_error('email', msg)    
        elif re.match(r'^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$',email):
            return True;
        else:  
            msg = "Invalid Email"
            self.add_error('email', msg)

    def clean_mobile_number(self):
    	md = super().clean()
    	mobile_number = md.get('mobile_number')
    	if re.match(r'^\d{10}$',mobile_number):
    		return True;
    	else:
    		msg = "Invalid Mobile number"
    		self.add_error('mobile_number', msg)

    def clean_password2(self):
    	pd=super().clean()
    	password1=pd.get('password1')
    	password2=pd.get('password2')
    	if password1==password2:
    		return True;
    	else:
    		msg="Password and confirm Password Mismatched!"
    		self.add_error('password2', msg)

class UserEditForm(forms.Form):

    user_id = forms.IntegerField(error_messages={'required': 'Empty or invalid User Type'})
    first_name = forms.CharField(error_messages={'required': 'Empty or invalid First name'})
    mobile_number = forms.CharField(required=False)
    def clean_mobile_number(self):
        md = super().clean()
        mobile_number = md.get('mobile_number')
        if re.match(r'^\d{10}$',mobile_number):
            return True;
        else:
            msg = "Invalid Mobile number"
            self.add_error('mobile_number', msg)
    


class RegForm(UserForm):
    user_id = forms.IntegerField(required=False)
    first_name = forms.CharField(error_messages={'required': 'Empty or invalid First name'})
    last_name = forms.CharField(required=False)
    dob = forms.DateField(required=False)
    email = forms.CharField(error_messages={'required': 'Empty or invalid Email'})
    password1 = forms.CharField(error_messages={'required': 'Empty or invalid password'})
    password2 = forms.CharField(error_messages={'required': 'Empty or invalid password'})
    gender = forms.IntegerField(required=False)
    mobile_number = forms.CharField(required=False)
    user_status = forms.IntegerField(required=False)

class ProfileEditForm(forms.Form):
    first_name = forms.CharField(error_messages={'required': 'Empty or invalid First name'})
    mobile_number = forms.CharField(required=False)
    def clean_mobile_number(self):
        md = super().clean()
        mobile_number = md.get('mobile_number')
        if re.match(r'^\d{10}$',mobile_number):
            return True;
        else:
            msg = "Invalid Mobile number"
            self.add_error('mobile_number', msg)
