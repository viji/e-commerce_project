from django.db import models

class UsersType(models.Model):
    
    code = models.CharField(max_length=50, null=True)
    name = models.CharField(max_length=250, null=True)
    status = models.SmallIntegerField(null=True)
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.IntegerField(null=True)
    
    class Meta:
        db_table="sih_user_type"

class Users(models.Model):
    
    user_code = models.CharField(max_length=250, null=True)
    user_type_id = models.IntegerField(null=True) 
    email = models.CharField(max_length=250, null=True)
    password = models.CharField(max_length=250, null=True)
    status =models.SmallIntegerField(null=True)
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.IntegerField(null=True)
    
    class Meta:
        db_table="sih_users"
        
class UserCodes(models.Model):
    
    class Meta:
        db_table ="sih_user_codes"
        
class UserProfile(models.Model):
    
    first_name = models.CharField(max_length=250, null=True)
    last_name = models.CharField(max_length=250, null=True)
    user_id = models.IntegerField(null=True)   
    # user_id = models.ForeignKey(Users, on_delete=models.CASCADE,null=True)   
    dob = models.DateField(null=True)
    gender = models.SmallIntegerField(null=True)
    contact_no = models.CharField(max_length=250, null=True)
    image = models.TextField(null=True)
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.IntegerField(null=True)
    
    class Meta:
        db_table="sih_user_profile"