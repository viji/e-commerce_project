from django.shortcuts import render, redirect  
from datetime import date, timedelta
from django.http import HttpResponseRedirect 
from django.http import HttpResponse
from django.conf import settings
import os.path

from common.views import admin_required
from common.views import user_login_required
from user.models import Users
from user.models import UsersType
from user.models import UserCodes
from user.models import UserProfile

from django.db.models.functions import Concat
import math
from datetime import date, timedelta
import datetime
from user.forms import UserForm
from user.forms import UserEditForm
from user.forms import RegForm
from user.forms import ProfileEditForm
from django.contrib import messages
import json
from django.http import JsonResponse


def user_login(request):
	email=password=""
	email_error=password_error=common_error=""
	user_info=user_profile_info=""
	if request.method == 'POST':
		if request.POST.get('email'):
			email = request.POST.get('email')
	   
		if request.POST.get('password'):
			password = request.POST.get('password')
	   
		if email and password:
			try:
				user_info = Users.objects.get(email=email,password=password)
			except:
				user_info=""
		   
			if user_info:
				
				user_type = UsersType.objects.get(id=user_info.user_type_id)
				user_profile_info = UserProfile.objects.get(user_id=user_info.id)
				
				request.session['loggedin_user_id']=user_info.id
				request.session['loggedin_user_code']=user_info.user_code
				request.session['loggedin_user_type']=user_type.code
				request.session['loggedin_user_email']=user_info.email
				request.session['loggedin_user_name']=user_profile_info.first_name+" "+user_profile_info.last_name
				if request.session['loggedin_user_type'] != "":
					if request.session['loggedin_user_type'] == "EMPLOYEE":
						return HttpResponseRedirect("/user_list")
					else:
						return HttpResponseRedirect("/home")
				# return HttpResponseRedirect("/home")
			else:
			 
				common_error="Invalid credential"
				
		else:
			common_error= "Please enter login credential"
			
	error_array={
					'email_error':email_error,
					'password_error':password_error,
					'common_error':common_error,
				}
	# request.session['loggedin_user_id']=login_user_info.user_id
	
	if 'loggedin_user_type' in request.session:
		if request.session['loggedin_user_type'] != "":
			if request.session['loggedin_user_type'] == "EMPLOYEE":
				return HttpResponseRedirect("/user_list")
			else:
				return HttpResponseRedirect("/home")
		else:
			return render(request,"accounts/login.html",{'error_array':error_array})
	else:
		return render(request,"accounts/login.html",{'error_array':error_array})
		
def user_logout(request):

	if 'loggedin_user_id' in request.session:
		del request.session['loggedin_user_id']
	if 'loggedin_user_code' in request.session:
		del request.session['loggedin_user_code']
	if 'loggedin_user_type' in request.session:
		del request.session['loggedin_user_type']
	if 'loggedin_user_email' in request.session:
		del request.session['loggedin_user_email']
	if 'loggedin_user_name' in request.session:
		del request.session['loggedin_user_name']
		
	if 'billing_address_info' in request.session:
		del request.session['billing_address_info']
	if 'shiping_address_info' in request.session:
		del request.session['shiping_address_info']
	if 'cart_coupon_code' in request.session:
		del request.session['cart_coupon_code']
	if 'cart_amount_info' in request.session:
		del request.session['cart_amount_info']
	if 'cart_product_info' in request.session:
		del request.session['cart_product_info']

	return HttpResponseRedirect("/home")
	
@admin_required  
def user_add(request):
    form=last_name=""
    user_type=UsersType.objects.all()
    if request.method=='POST':
        form = UserForm(request.POST) 
        print(form.errors)
        if form.is_valid():
            if request.POST.get('user_id'):
                user_id = request.POST.get('user_id')
            if request.POST.get('email'):
                email = request.POST.get('email')
            if request.POST.get('password1'):
                password1 = request.POST.get('password1')
            if request.POST.get('password2'):
                password2 = request.POST.get('password2')
            if request.POST.get('first_name'):
                first_name = request.POST.get('first_name').strip()
                print(first_name)
            if request.POST.get('last_name'):
                last_name = request.POST.get('last_name').strip()
                print(last_name)
            if request.POST.get('dob'):
                dob = datetime.datetime.strptime(request.POST.get('dob','') , '%m/%d/%Y').strftime('%Y-%m-%d')
                print(dob)
            if request.POST.get('gender'):
                gender = request.POST.get('gender')
            if request.POST.get('mobile_number'):
                mobile_number = request.POST.get('mobile_number')
            if request.POST.get('user_status'):
                user_status = request.POST.get('user_status')
            user_id_val=UserCodes(id=None)
            user_id_val.save()
            id_last = UserCodes.objects.latest('id')
            user_code="USR000"+str(id_last.id)
            created_on = datetime.datetime.utcnow()
            created_by = request.session['loggedin_user_id']
            modified_on = None
            modified_by = None
            users_values=Users(user_code=user_code,user_type_id=user_id,email=email,password=password2,status=user_status,created_on=created_on,created_by=created_by,modified_on=modified_on,modified_by=modified_by)
            users_values.save()
            user_id_last = Users.objects.latest('id')
            user_profile_values=UserProfile(first_name=first_name,last_name=last_name,user_id=user_id_last.id,dob=dob,gender=gender,contact_no=mobile_number,image=None,created_on=datetime.datetime.utcnow(),created_by=request.session['loggedin_user_id'],modified_on=None,modified_by=None)
            user_profile_values.save()
            messages.success(request, 'User added successfully.')
            return redirect('/user_list')


    else:  
        form = UserForm()



    
    return render(request,"user_add.html",{'user_type':user_type,'forms':form})

@admin_required   
def user_edit(request,id):
    user_profile_values=UserProfile.objects.get(id=id)
    user_values=Users.objects.get(id=id)
    user_types=UsersType.objects.all()
    form=last_name=first_name=""
    if request.method=='POST':
        form = UserEditForm(request.POST) 
        print(form.errors)
        if form.is_valid():
            if request.POST.get('user_id'):
                user_id = request.POST.get('user_id')
                
            if request.POST.get('first_name'):
                first_name = request.POST.get('first_name').strip()
               
            if request.POST.get('last_name'):
                last_name = request.POST.get('last_name').strip()
        
            if request.POST.get('dob'):
                dob = datetime.datetime.strptime(request.POST.get('dob','') , '%m/%d/%Y').strftime('%Y-%m-%d')
                
            if request.POST.get('gender'):
                gender = request.POST.get('gender')
            if request.POST.get('mobile_number'):
                mobile_number = request.POST.get('mobile_number')
            if request.POST.get('user_status'):
                user_status = request.POST.get('user_status')
            modified_on = datetime.datetime.utcnow()
            modified_by = request.session['loggedin_user_id']
            users_values=Users.objects.filter(id=id).update(user_type_id=user_id,status=user_status,modified_on=modified_on,modified_by=modified_by)
            user_id_last = Users.objects.latest('id')
            user_profile_values=UserProfile.objects.filter(user_id=id).update(first_name=first_name,last_name=last_name,dob=dob,gender=gender,contact_no=mobile_number,image=None,modified_on=datetime.datetime.utcnow(),modified_by=request.session['loggedin_user_id'])
            messages.success(request, 'User Updated successfully.')
            return redirect('/user_list')


    else:  
        form = UserEditForm()

    return render(request,"user_edit.html",{'user_profile_values':user_profile_values,'user_values':user_values,'user_types':user_types,'forms':form})
 
@admin_required   
def user_list(request):

    sql = where_str =varSortOrder=varSortCol=""
    # sorting starts
    sort_str=""
    varSortColumn="created_on"
    varSortOrderVal="DESC"
    if request.POST.get('varSortOrder'):
        varSortOrderVal = request.POST.get('varSortOrder')
    
    if request.POST.get('varSortCol'):
        varSortColumn = request.POST.get('varSortCol')
       
    # if 'varSortOrder' in request.session: 
        # varSortOrder = request.session['varSortOrder']
    # else:
        # varSortOrder = varSortOrder
    # if 'varSortCol' in request.session: 
        # varSortCol = request.session['varSortCol']
    # else:
        # varSortCol = varSortCol
    sort_str = " ORDER BY " + varSortColumn + " " + varSortOrderVal
    
    # if varSortOrderVal == "ASC":
        # sort_str=varSortColumn
    # elif varSortOrderVal == "DESC":
        # sort_str='-'+varSortColumn
        
    # sorting ends
    
    # search start
    search_keyword=search_status=search_user_type=""
    query_data = {}
    user_query_data = {}
    srh_usr_type_usr_ids=[]
    srh_usr_status_usr_ids=[]
    srh_keywrd_usr_ids=[]
    user_ids=""
    if request.POST.get('search_user_type',''):
        search_user_type = request.POST.get('search_user_type')
        if search_user_type !="0":
            for user_info in Users.objects.filter(user_type_id=search_user_type):
                srh_usr_type_usr_ids.append(user_info.id)
            if len(srh_usr_type_usr_ids)==0:
                srh_usr_type_usr_ids.append(0)
            
    if request.POST.get('search_status',''):
        search_status = request.POST.get('search_status')
        if search_status !="0" :
            for user_info in Users.objects.filter(status=search_status):
                srh_usr_status_usr_ids.append(user_info.id)
            if len(srh_usr_status_usr_ids)==0:
                srh_usr_status_usr_ids.append(0)
         
    if request.POST.get('search_keyword',''):
        search_keyword=request.POST.get('search_keyword','')
        if search_keyword:
            user_query_data['keyword__contains'] = search_keyword
            for user_info in Users.objects.all().annotate(keyword=Concat('email','user_code')).filter(**user_query_data):
                srh_keywrd_usr_ids.append(user_info.id)
            for user_pro_info in UserProfile.objects.all().annotate(keyword=Concat('first_name','last_name','contact_no')).filter(**user_query_data):
                if user_pro_info.user_id not in srh_keywrd_usr_ids:
                    srh_keywrd_usr_ids.append(user_pro_info.user_id)
            if len(srh_keywrd_usr_ids)==0:
                srh_keywrd_usr_ids.append(0)
                
    user_ids = set(srh_usr_type_usr_ids) | set(srh_keywrd_usr_ids) | set(srh_usr_status_usr_ids)
    joined_string=""
    
    if user_ids:
        query_data['user_id__in'] = user_ids
        joined_string = ','.join(str(ids) for ids in user_ids)
        where_str =" AND usr.id IN ("+str(joined_string)+")"
      
    # search end
   
    #paginations starts
    
    varPageNo = settings.PAGE_NO
    rec_count = 5

    if request.POST.get('count'):
        rec_count = request.POST.get('count')
        
    if request.POST.get('varPageNo'):
        varPageNo = request.POST.get('varPageNo')
        
    pageNo = int(varPageNo) - int(1)

    start_from = int(pageNo) * int(rec_count)

    pagination = " LIMIT " + str(start_from) + "," + str(rec_count)

    # page_cnt=UserProfile.objects.all().filter(**query_data).order_by(sort_str)
    # print(page_cnt.query)
    
    page_sql = "SELECT usr.*,pro.first_name,pro.last_name,pro.dob,pro.gender,pro.contact_no,pro.image,usr_tpy.name,crt_pro.first_name as created_by_name FROM sih_users as usr ,sih_user_type as usr_tpy,sih_user_profile as pro,sih_user_profile as crt_pro WHERE usr.id = pro.user_id AND usr_tpy.id =usr.user_type_id AND crt_pro.created_by = crt_pro.id" + where_str + sort_str
   
    page_cnt=UserProfile.objects.raw(page_sql)
    
    rec_count_value=settings.REC_COUNT


    page_cnt = len(page_cnt)
    no = int(page_cnt)/int(rec_count)

    total_pages = math.ceil(no);
    startRec=1
    endRec=""
    if int(varPageNo) == 1:
        if int(page_cnt) < int(rec_count):
            endRec = page_cnt
        else:
            endRec = rec_count
    else:
        startRec = int(rec_count) * int(pageNo)
        endRec   = int(startRec) + int(rec_count)
        if int(endRec) > int(page_cnt):
            endRec = page_cnt
        
        startRec += 1

    #paginations ends
    sql = "SELECT usr.*,pro.first_name,pro.last_name,pro.dob,pro.gender,pro.contact_no,pro.image,usr_tpy.name as user_type,crt_pro.first_name as created_by_name FROM sih_users as usr ,sih_user_type as usr_tpy,sih_user_profile as pro,sih_user_profile as crt_pro WHERE usr.id = pro.user_id AND usr_tpy.id =usr.user_type_id AND crt_pro.created_by = crt_pro.id" + where_str + sort_str+ pagination
    user_profile = UserProfile.objects.raw(sql)
  
    # user_profile = UserProfile.objects.all().filter(**query_data).order_by(sort_str)[int(start_from):start_from+int(rec_count)]
    
    user_type = UsersType.objects.all()
    
    user_data = Users.objects.all()
	 
    search_val={
        
        'search_user_type':int('0' + search_user_type),
        'search_status' :search_status,
        'search_keyword':search_keyword,
        
    
    }
    return render(request,"user_list.html",{'user_profile':user_profile, 'user_type':user_type, 'user_data':user_data, 'endRec':endRec,'startRec':startRec,'total_pages':range(total_pages),'page_cnt':page_cnt,'varPageNo':int(varPageNo),'total_no':int(total_pages),'rec_count':int(rec_count),'rec_count_value':rec_count_value,'tatal_rec' : page_cnt,'varSortCol':varSortColumn,'varSortOrder':varSortOrderVal,'search_val':search_val})
       
@admin_required   
def user_view(request,id):

    
    user_profile = UserProfile.objects.get(id=id)
    user_type = UsersType.objects.get(id=id)
	
    return render(request,"user_view.html",{'user_profile':user_profile, 'user_type':user_type})
  
 
def user_register(request):
    form=last_name=""
    user_type=UsersType.objects.all()
    if request.method=='POST':
        form = RegForm(request.POST) 
       
        user_type_id = 2
        user_status = 1
        if form.is_valid():
            if request.POST.get('email'):
                email = request.POST.get('email')
            if request.POST.get('password1'):
                password1 = request.POST.get('password1')
            if request.POST.get('password2'):
                password2 = request.POST.get('password2')
            if request.POST.get('first_name'):
                first_name = request.POST.get('first_name').strip()
                print(first_name)
            if request.POST.get('last_name'):
                last_name = request.POST.get('last_name').strip()
               
            if request.POST.get('dob'):
                dob = datetime.datetime.strptime(request.POST.get('dob','') , '%m/%d/%Y').strftime('%Y-%m-%d')
               
            if request.POST.get('gender'):
                gender = request.POST.get('gender')
            if request.POST.get('mobile_number'):
                mobile_number = request.POST.get('mobile_number')
            user_id_val=UserCodes(id=None)
            user_id_val.save()
           
            id_last = UserCodes.objects.latest('id')
            user_code="USR000"+str(id_last.id)
            created_on = datetime.datetime.utcnow()
            created_by = 2
            modified_on = None
            modified_by = None
            users_values=Users(user_code=user_code,user_type_id=user_type_id,email=email,password=password2,status=user_status,created_on=created_on,created_by=created_by,modified_on=modified_on,modified_by=modified_by)
            users_values.save()
            user_id_last = Users.objects.latest('id')
            user_profile_values=UserProfile(first_name=first_name,last_name=last_name,user_id=user_id_last.id,dob=dob,gender=gender,contact_no=mobile_number,image=None,created_on=datetime.datetime.utcnow(),created_by=2,modified_on=None,modified_by=None)
            user_profile_values.save()
            messages.success(request, 'Registered successfully!')
            return redirect('/user_register')


    else:  
        form = RegForm()
    
    return render(request,"accounts/register.html",{'forms':form})
    
@user_login_required
def user_profile(request,id):
    user_profile = UserProfile.objects.get(user_id=id)
    user_details = Users.objects.get(id=id)
    return render(request,"accounts/user_profile.html",{'user_profile':user_profile,'user_details':user_details})
@user_login_required
def user_profile_edit(request,id):
    user_profile_values=UserProfile.objects.get(user_id=id)
    user_values=Users.objects.get(id=id)
    user_types=UsersType.objects.all()
    form=last_name=first_name=""
    if request.method=='POST':
        form =ProfileEditForm(request.POST) 
        print(form.errors)
        if form.is_valid():
            if request.POST.get('first_name'):
                first_name = request.POST.get('first_name').strip()
               
            if request.POST.get('last_name'):
                last_name = request.POST.get('last_name').strip()
        
            if request.POST.get('dob'):
                dob = datetime.datetime.strptime(request.POST.get('dob','') , '%m/%d/%Y').strftime('%Y-%m-%d')
                
            if request.POST.get('gender'):
                gender = request.POST.get('gender')
            if request.POST.get('mobile_number'):
                mobile_number = request.POST.get('mobile_number')
            if request.POST.get('user_status'):
                user_status = request.POST.get('user_status')
            modified_on = datetime.datetime.utcnow()
            modified_by = request.session['loggedin_user_id']
            users_values=Users.objects.filter(id=id).update(status=user_status,modified_on=modified_on,modified_by=modified_by)
            user_profile_val=UserProfile.objects.filter(user_id=id).update(first_name=first_name,last_name=last_name,dob=dob,gender=gender,contact_no=mobile_number,image=None,modified_on=datetime.datetime.utcnow(),modified_by=request.session['loggedin_user_id'])
            messages.success(request, 'Profile Details Updated successfully.')
            return redirect('/user_profile/'+str(user_profile_values.user_id))


    else:  
        form = ProfileEditForm()

    return render(request,"accounts/user_profile_edit.html",{'user_profile_values':user_profile_values,'user_values':user_values,'user_types':user_types,'forms':form})
 
def forgot_password(request):
    response_data={}
    email=user_details=""
    password=common_error=""
    if request.method == "POST":
        if request.POST.get('email'):
            email = request.POST.get('email').strip()
            try:
                user_details=Users.objects.get(email=email)
            except:
                user_details=""
            print(user_details)
            if user_details:
                print("yes")
                response_data['result'] = "success"
                response_data['password'] = user_details.password
            else:
                print("No")
                response_data['result'] = "Failed"
    '''if email:
        user_details=Users.objects.get(email=email)
        if user_details:
            password=user_details.password
        else:
            common_error="Invalid Email Please Register!"

    response_data['common_error'] = common_error
    '''
    
    return JsonResponse(response_data) 
