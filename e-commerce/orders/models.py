from django.db import models

class Orders(models.Model):
    
    order_code = models.CharField(max_length=250, null=True)
    user_id =  models.IntegerField(null=True)
    tax = models.FloatField(null=True)
    shipping_cost = models.FloatField(null=True)
    discount_cost = models.FloatField(null=True)
    total = models.FloatField(null=True)
    balance_unpaid = models.FloatField(null=True)
    payment_type =models.IntegerField(null=True)
    subscription =models.IntegerField(null=True)
    status =models.SmallIntegerField(null=True)
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.IntegerField(null=True)
    
    class Meta:
        db_table="sih_orders"
         
class OrderCodes(models.Model):
    
    class Meta:
        db_table ="sih_order_codes"
 
class OrderStatus(models.Model):
    
    status_id = models.IntegerField(null=True) 
    status_name = models.CharField(max_length=100, null=True)
    
    class Meta:
        db_table="sih_order_status"
                              
class OrderProduct(models.Model):

    order_id = models.IntegerField(null=True)
    prd_id = models.IntegerField(null=True)
    prd_name = models.CharField(max_length=250, null=True)
    package_id = models.IntegerField(null=True)
    prd_qty = models.IntegerField(null=True)
    prd_weight = models.IntegerField(null=True)
    prd_price = models.FloatField(null=True)
    prd_subscription =models.IntegerField(null=True)
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.IntegerField(null=True)
    
    class Meta:
        db_table ="sih_order_products"
                   
class OrderCoupon(models.Model):

    order_id = models.IntegerField(null=True)
    coupon_id = models.IntegerField(null=True)
    coupon_titile = models.CharField(max_length=250, null=True)
    discount_percentage = models.IntegerField(null=True)
    actual_amount = models.FloatField(null=True)
    discount_amount = models.FloatField(null=True)
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    
    class Meta:
        db_table ="sih_order_coupon"      
        
class OrderBillingAddress(models.Model):

    order_id = models.IntegerField(null=True)    
    address1 = models.CharField(max_length=250, null=True)
    address2 = models.CharField(max_length=250, null=True)
    city = models.CharField(max_length=100, null=True)
    state = models.CharField(max_length=100, null=True)
    postal_code = models.CharField(max_length=100, null=True)
    country = models.CharField(max_length=100, null=True)
    email = models.CharField(max_length=250, null=True)
    mobile = models.CharField(max_length=250, null=True)
    land_mark = models.CharField(max_length=250, null=True)
    
    class Meta:
        db_table ="sih_order_bill_address"
                       
class OrderShippingAddress(models.Model):

    order_id = models.IntegerField(null=True)    
    address1 = models.CharField(max_length=250, null=True)
    address2 = models.CharField(max_length=250, null=True)
    city = models.CharField(max_length=100, null=True)
    state = models.CharField(max_length=100, null=True)
    postal_code = models.CharField(max_length=100, null=True)
    country = models.CharField(max_length=100, null=True)
    email = models.CharField(max_length=250, null=True)
    mobile = models.CharField(max_length=250, null=True)
    land_mark = models.CharField(max_length=250, null=True)
    
    class Meta:
        db_table ="sih_order_ship_address"

class OrderShippingProcess(models.Model):

    order_id = models.IntegerField(null=True)    
    shipped_date = models.DateField(null=True)    
    in_hand_date = models.DateField(null=True)    
    shipping_method_id = models.IntegerField(null=True)    
    tracking_numbers = models.TextField(null=True)
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.IntegerField(null=True)
    
    class Meta:
        db_table ="sih_order_shipping"
        
class OrderLog(models.Model):

    order_id = models.IntegerField(null=True)
    field = models.CharField(max_length=250, null=True)    
    old_value = models.CharField(max_length=250, null=True)    
    new_value = models.CharField(max_length=250, null=True) 
    description = models.TextField(null=True)
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    
    class Meta:
        db_table ="sih_order_log"
               