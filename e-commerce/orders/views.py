from django.shortcuts import render, redirect  
from datetime import date, timedelta
from django.http import HttpResponseRedirect 
from django.http import HttpResponse
from django.conf import settings
import os.path
import datetime
import math
from django.contrib import messages
from django.db.models.functions import Concat
from common.views import admin_required
from common.views import user_login_required
from orders.models import OrderCodes
from orders.models import Orders
from cart.models import Subscription
from orders.models import OrderStatus
from orders.models import OrderProduct
from orders.models import OrderCoupon
from orders.models import OrderBillingAddress
from orders.models import OrderShippingAddress
from orders.models import OrderShippingProcess
from orders.models import OrderLog
from orders.forms import ShipDateaddForm
from cart.models import Cart
from user.models import Users
from user.models import UsersType
from user.models import UserCodes
from user.models import UserProfile
from coupon.models import Coupon
from product.models import Product

@user_login_required
def order_confirmation(request):
	ship_land_mark=bill_land_mark=""
	if "loggedin_user_id" in request.session:
		if request.session['loggedin_user_id']:
			if request.method == "POST":
				if request.POST.get('bill_address1'):
					bill_address1 = request.POST.get('bill_address1').strip()
				if request.POST.get('bill_address2'):
					bill_address2 = request.POST.get('bill_address2').strip()
				if request.POST.get('bill_city'):
					bill_city = request.POST.get('bill_city').strip()
				if request.POST.get('bill_state'):
					bill_state = request.POST.get('bill_state').strip()
				if request.POST.get('bill_postal_code'):
					bill_postal_code = request.POST.get('bill_postal_code').strip()
				if request.POST.get('bill_country'):
					bill_country = request.POST.get('bill_country').strip()
				if request.POST.get('bill_mobile_number'):
					bill_mobile_number = request.POST.get('bill_mobile_number').strip()
				if request.POST.get('bill_land_mark'):
					bill_land_mark = request.POST.get('bill_land_mark').strip()
				
				if request.POST.get('ship_address1'):
					ship_address1 = request.POST.get('ship_address1').strip()
				if request.POST.get('ship_address2'):
					ship_address2 = request.POST.get('ship_address2').strip()
				if request.POST.get('ship_city'):
					ship_city = request.POST.get('ship_city').strip()
				if request.POST.get('ship_state'):
					ship_state = request.POST.get('ship_state').strip()
				if request.POST.get('ship_postal_code'):
					ship_postal_code = request.POST.get('ship_postal_code').strip()
				if request.POST.get('ship_country'):
					ship_country = request.POST.get('ship_country').strip()
				if request.POST.get('ship_mobile_number'):
					ship_mobile_number = request.POST.get('ship_mobile_number').strip()
				if request.POST.get('ship_land_mark'):
					ship_land_mark = request.POST.get('ship_land_mark').strip()
					
				shiping_address_arr={
					'ship_address1':ship_address1,
					'ship_address2':ship_address2,
					'ship_city':ship_city,
					'ship_state':ship_state,
					'ship_postal_code':ship_postal_code,
					'ship_country':ship_country,
					'ship_mobile_number':ship_mobile_number,
					'ship_land_mark':ship_land_mark,
				
				}	
				billing_address_arr={
					'bill_address1':bill_address1,
					'bill_address2':bill_address2,
					'bill_city':bill_city,
					'bill_state':bill_state,
					'bill_postal_code':bill_postal_code,
					'bill_country':bill_country,
					'bill_mobile_number':bill_mobile_number,
					'bill_land_mark':bill_land_mark,
					}	
				
				request.session["billing_address_info"] = billing_address_arr
				request.session["shiping_address_info"] = shiping_address_arr
		else:
			return HttpResponseRedirect("/cart")
	else:
		return HttpResponseRedirect("/cart")

	return render(request,"order_confirmation.html",{'billing_address_info':request.session["billing_address_info"],'shiping_address_info':request.session["shiping_address_info"] })
	
@user_login_required
def order_create(request):
	product_array=""
	product_list=[]
	cart_ids=[]
			
	if "loggedin_user_id" in request.session:
		if request.session['loggedin_user_id']:
			
			#order code generate
			
			OrderCodes(id=None).save()
			
			latest_data = OrderCodes.objects.latest('id')
			
			order_code="ORD000"+str(latest_data.id)
			
			discount_cost=request.session["cart_amount_info"]['discount_amt']
			total=request.session["cart_amount_info"]['over_all_total']
			delivery_charge=request.session["cart_amount_info"]['delivery_charge']
			tax = 0
			
			#order creation
			
			Orders(order_code=order_code,user_id = request.session['loggedin_user_id'],tax=tax,shipping_cost=delivery_charge,discount_cost=discount_cost,total=total,balance_unpaid=total,status=1,created_on = datetime.datetime.utcnow(),created_by = request.session['loggedin_user_id'] ).save()
			
			#order id get
			
			latest_order = Orders.objects.latest('id')
			
			#order product creation
			for key,product in request.session["cart_product_info"].items():
				prd_price=0
				
				if product['offer_price']:
					prd_price = product['offer_price']
				elif product['actual_price']:	
					prd_price = product['actual_price']
				print(prd_price)	
				product_array = OrderProduct(order_id=latest_order.id,prd_id=product['product_id'],prd_name=product['name'],package_id=product['pack_id'],prd_qty=product['qty'],prd_weight=product['weight'],prd_price=prd_price,created_on = datetime.datetime.utcnow(),created_by = request.session['loggedin_user_id'],modified_on=None,modified_by=None)
				
				product_list.append(product_array)
				print(product_list)
			if product_list:
				OrderProduct.objects.bulk_create(product_list)
			
			#coupon code creation	
			if 'cart_coupon_code' in request.session:
				if request.session['cart_coupon_code']:
				
					coupon_data=Coupon.objects.get(title=request.session['cart_coupon_code'])
					
					actual_amount = request.session["cart_amount_info"]["sub_total"]
					discount_amount = request.session["cart_amount_info"]["discount_amt"]
					
					OrderCoupon(order_id=latest_order.id,coupon_id=coupon_data.id,coupon_titile=coupon_data.title,discount_percentage=coupon_data.offer_percentage,actual_amount=actual_amount,discount_amount=discount_amount,created_on = datetime.datetime.utcnow(),created_by = request.session['loggedin_user_id']).save()
			
			#OrderBillingAddress creation
			if 'billing_address_info' in request.session:
				bill_address1 = request.session["billing_address_info"]['bill_address1']
				bill_address2 = request.session["billing_address_info"]['bill_address2']
				bill_city = request.session["billing_address_info"]['bill_city']
				bill_state = request.session["billing_address_info"]['bill_state']
				bill_postal_code = request.session["billing_address_info"]['bill_postal_code']
				bill_country = request.session["billing_address_info"]['bill_country']
				bill_mobile_number = request.session["billing_address_info"]['bill_mobile_number']
				bill_land_mark = request.session["billing_address_info"]['bill_land_mark']
				
				OrderBillingAddress(order_id=latest_order.id,address1=bill_address1,address2=bill_address2,city=bill_city,state=bill_state,postal_code=bill_postal_code,country=bill_country,email="",mobile=bill_mobile_number,land_mark=bill_land_mark).save()
				
			#OrderShippingAddress creation
			if 'shiping_address_info' in request.session:
				ship_address1 = request.session["shiping_address_info"]['ship_address1']
				ship_address2 = request.session["shiping_address_info"]['ship_address2']
				ship_city = request.session["shiping_address_info"]['ship_city']
				ship_state = request.session["shiping_address_info"]['ship_state']
				ship_postal_code = request.session["shiping_address_info"]['ship_postal_code']
				ship_country = request.session["shiping_address_info"]['ship_country']
				ship_mobile_number = request.session["shiping_address_info"]['ship_mobile_number']
				ship_land_mark = request.session["shiping_address_info"]['ship_land_mark']
				
				OrderShippingAddress(order_id=latest_order.id,address1=ship_address1,address2=ship_address2,city=ship_city,state=ship_state,postal_code=ship_postal_code,country=ship_country,email="",mobile=ship_mobile_number,land_mark=ship_land_mark).save()
				
			# delete cart items from table
			
			for key,cart_info in request.session["cart_product_info"].items():	
				cart_ids.append(cart_info['id'])
			if cart_ids:	
				Cart.objects.filter(id__in=cart_ids).delete()
				
			return HttpResponseRedirect("/order_list")
			
		else:
			return HttpResponseRedirect("/cart")
	else:
		return HttpResponseRedirect("/cart")	
		
	return render(request,"order_confirmation.html",)
	
@user_login_required
def order_list(request):
	sql = where_str =""
	query_data={}
	search_keyword=search_status=""

	#filter data set start
	if request.session['loggedin_user_id']:
		if request.session['loggedin_user_type'] == 'CUSTOMER':
			created_by=request.session['loggedin_user_id']
			query_data["created_by"] = created_by
		if request.POST.get('search_keyword',''):
			search_keyword=request.POST.get('search_keyword','')
			query_data['keyword__contains'] = search_keyword
		if request.POST.get('search_status',''):
			search_status=request.POST.get('search_status','')
			query_data['search_status'] = search_status
		#filter data set end    
			
		# sorting starts
		sort_str=""
		varSortColumn="created_on"
		varSortOrderVal="DESC"
		if request.POST.get('varSortOrder'):
			varSortOrderVal = request.POST.get('varSortOrder')

		if request.POST.get('varSortCol'):
			varSortColumn = request.POST.get('varSortCol')

		if varSortOrderVal == "ASC":
			sort_str=varSortColumn
		elif varSortOrderVal == "DESC":
			sort_str='-'+varSortColumn

		#paginations starts

		varPageNo = settings.PAGE_NO
		rec_count = 5

		if request.POST.get('count'):
			rec_count = request.POST.get('count')
			
		if request.POST.get('varPageNo'):
			varPageNo = request.POST.get('varPageNo')
			
		pageNo = int(varPageNo) - int(1)

		start_from = int(pageNo) * int(rec_count)

		pagination = " LIMIT " + str(start_from) + "," + str(rec_count)

		page_cnt=Orders.objects.all().annotate(keyword=Concat('order_code','order_code')).filter(**query_data).order_by(sort_str)
		# print(page_cnt.query)
		rec_count_value=settings.REC_COUNT


		page_cnt = len(page_cnt)
		no = int(page_cnt)/int(rec_count)

		total_pages = math.ceil(no);
		startRec=1
		endRec=""
		if int(varPageNo) == 1:
			if int(page_cnt) < int(rec_count):
				endRec = page_cnt
			else:
				endRec = rec_count
		else:
			startRec = int(rec_count) * int(pageNo)
			endRec   = int(startRec) + int(rec_count)
			if int(endRec) > int(page_cnt):
				endRec = page_cnt
			
			startRec += 1

		#paginations ends


		order_data = Orders.objects.all().annotate(keyword=Concat('order_code','order_code')).filter(**query_data).order_by(sort_str)[int(start_from):start_from+int(rec_count)]

		order_status = OrderStatus.objects.all()

		search_val={'search_keyword':search_keyword,'search_status':search_status}


		return render(request,"order_list.html",{'order_data':order_data,'search_val':search_val,'endRec':endRec,'startRec':startRec,'total_pages':range(total_pages),'page_cnt':page_cnt,'varPageNo':int(varPageNo),'total_no':int(total_pages),'rec_count':int(rec_count),'rec_count_value':rec_count_value,'tatal_rec' : page_cnt,'varSortCol':varSortColumn,'varSortOrder':varSortOrderVal,'order_status':order_status})
	else:
		return redirect("/user_login")

@user_login_required
def order_view(request, id):
	if request.session['loggedin_user_id']:
		shipped_vals=""
		order_info = Orders.objects.get(id=id)
		shipping_address = OrderShippingAddress.objects.get(order_id=id)
		billing_address = OrderBillingAddress.objects.get(order_id=id)
		products_details = OrderProduct.objects.filter(order_id=id)
		
		try:
			coupon_details = OrderCoupon.objects.get(order_id=id)
		except:
			coupon_details=""
		try:
			order_process_details = OrderShippingProcess.objects.get(order_id=id)
		except:
			order_process_details=""
		
		sub_total=0
		if products_details:
			for value in products_details:
				total=value.prd_qty * value.prd_price
				sub_total = sub_total+total
		
		usr_type_names=[]
		user_type=UsersType.objects.all()
		for users in user_type:
			usr_type_names.append(users.name)
		print(usr_type_names)
		form=""
		if request.method=='POST':
			form = ShipDateaddForm(request.POST) 
			print(form.errors)
			if form.is_valid():
				if request.POST.get('shipped_date'):
					shipped_date = datetime.datetime.strptime(request.POST.get('shipped_date','') , '%m/%d/%Y').strftime('%Y-%m-%d')
				if request.POST.get('in_hand_date'):
					in_hand_date = datetime.datetime.strptime(request.POST.get('in_hand_date','') , '%m/%d/%Y').strftime('%Y-%m-%d')
				if request.POST.get('shipping_method_id'):
					shipping_method_id = request.POST.get('shipping_method_id')
				if request.POST.get('tracking_numbers'):
					tracking_numbers = request.POST.get('tracking_numbers')
				created_on = datetime.datetime.utcnow()
				created_by = request.session['loggedin_user_id']
				modified_on = None
				modified_by = None
				
				order_ship_values=OrderShippingProcess.objects.filter(order_id=id).exists()
				if order_ship_values:
					OrderShippingProcess.objects.filter(order_id=order_info.id).update(shipped_date=shipped_date,in_hand_date=in_hand_date,shipping_method_id=shipping_method_id,tracking_numbers=tracking_numbers,modified_on=datetime.datetime.utcnow(),modified_by=request.session['loggedin_user_id'])
					shipped_vals=OrderShippingProcess.objects.get(order_id=id)
					#messages.success(request, 'Shipped details updated Successfully!')
					#return redirect('/order_view/'+str(id))
				else:
					shipped_details=OrderShippingProcess(order_id=order_info.id,shipped_date=shipped_date,in_hand_date=in_hand_date,shipping_method_id=shipping_method_id,tracking_numbers=tracking_numbers,created_on=created_on,created_by=created_by,modified_on=modified_on,modified_by=modified_by)
					shipped_details.save()
					orders_val=Orders.objects.filter(id=id).update(status=2,modified_on=datetime.datetime.utcnow(),modified_by=request.session['loggedin_user_id'])
					order_log=OrderLog(order_id=order_info.id,field='status',old_value='new',new_value='shipped',description="new to shipped",created_on=created_on,created_by=created_by)
					order_log.save()
					shipped_vals=OrderShippingProcess.objects.get(order_id=id)
					#messages.success(request, 'Shipped details added Successfully!')
		try:			#return redirect('/order_view/'+str(id))
			shipped_vals=OrderShippingProcess.objects.get(order_id=id)
		except:
			shipped_vals=""
		#print(shipped_vals)
		return render(request,"order_view.html",{'order_info':order_info,'shipping_address':shipping_address,'billing_address':billing_address,'coupon_details':coupon_details,'products_details':products_details,'order_process_details':order_process_details,'sub_total':sub_total,'usr_type_names':usr_type_names,'forms':form,'shipped_vals':shipped_vals})
	else:
		return redirect('/user_login')

@user_login_required
def order_status_update(request,order_id,status_id):
	old_value=new_value=""
	orders_val= Orders.objects.get(id=order_id)
	if orders_val.status == 1:
		old_value="new"
	elif orders_val.status == 2:
		old_value ="shipped"
	if status_id == 3:
		new_value = "closed"
	elif status_id == 4:
		new_value = "cancelled"
	else:
		new_value ="return"
	description= "status changed from "+old_value+ " to " + new_value
	created_on = datetime.datetime.utcnow()
	created_by = request.session['loggedin_user_id']
	if "loggedin_user_id" in request.session:
		if request.session['loggedin_user_id']:
			if  request.session['loggedin_user_type'] == 'CUSTOMER':
				if request.session['loggedin_user_id']== orders_val.created_by:
					Orders.objects.filter(id=order_id).update(status=status_id,modified_on=datetime.datetime.utcnow(),modified_by=request.session['loggedin_user_id'])
					OrderLog(order_id=order_id,field="status",old_value=old_value,new_value=new_value,description=description,created_on=created_on,created_by=created_by).save()
				else:
					return redirect("/order_view/"+str(order_id))
			elif request.session['loggedin_user_type'] == "EMPLOYEE":
				Orders.objects.filter(id=order_id).update(status=status_id,modified_on=datetime.datetime.utcnow(),modified_by=request.session['loggedin_user_id'])
				OrderLog(order_id=order_id,field="status",old_value=old_value,new_value=new_value,description=description,created_on=created_on,created_by=created_by).save()

		else:
			return redirect("/user_login")


	return redirect("/order_view/"+str(order_id))


def subscription_list(request):
	subs_values=Subscription.objects.all()
	prd_values=Product.objects.all()
	return render(request,"subscription_list.html",{'subs_values':subs_values,'prd_values':prd_values})

