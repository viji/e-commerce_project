from django import forms
from django.core.exceptions import ValidationError
from orders.models import OrderShippingProcess
import datetime

class ShipDateaddForm(forms.Form):   
    shipped_date = forms.DateField(error_messages={'required': 'Empty or invalid Shipping Date'})    
    in_hand_date = forms.DateField(error_messages={'required': 'Empty or invalid Inhand date'})    
    shipping_method_id = forms.IntegerField(error_messages={'required': 'Empty or invalid Shipping Method'})    
    tracking_numbers = forms.CharField(error_messages={'required': 'Empty or invalid Tracking Number'})

    def clean_shipped_date(self):
    	ed = super().clean()
    	shipped_date= ed.get('shipped_date')
    	return shipped_date

    def clean_in_hand_date(self):
        ed = super().clean() 
        in_hand_date= ed.get('in_hand_date')
        shipped_date= self.clean_shipped_date()
        print(shipped_date)
        print(in_hand_date)
        #ship_date_val=datetime.datetime.combine(shipped_date, datetime.time(0, 0))
        #inhand_date_val=datetime.datetime.combine(in_hand_date, datetime.time(0, 0))
        #today_date = datetime.datetime.utcnow()
        if in_hand_date <= shipped_date:
            msg = "Invalid In-hand date"
            self.add_error('in_hand_date', msg)
        else:
            return True
    