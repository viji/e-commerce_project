from django.db import models

class ProductCodes(models.Model): 
    
    class Meta:
        db_table="sih_product_codes"
        
class Product(models.Model):
    
    product_code    = models.CharField(max_length=250, null=True)
    product_group_id =models.IntegerField(null=True)
    category_id =models.IntegerField(null=True)
    pack_id =models.IntegerField(null=True)
    name = models.CharField(max_length=250, null=True)
    seo_name = models.CharField(max_length=250, null=True)
    mat_code = models.CharField(max_length=250, null=True)
    weight =models.IntegerField(null=True)
    offer_price =models.FloatField(null=True)
    actual_price =models.FloatField(null=True)
    gtin =models.IntegerField(null=True)
    description =models.TextField(null=True)
    status =models.SmallIntegerField(null=True)
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.IntegerField(null=True)
    
    class Meta:
        db_table="sih_product"


class ProductImage(models.Model):
 
    product_id =models.IntegerField(null=True)
    image =models.TextField(null=True)
    is_default =models.SmallIntegerField(null=True)
    
    class Meta:
        db_table="sih_product_image"
        

class ProductPackageType(models.Model):
    code = models.CharField(max_length=250, null=True)
    name = models.CharField(max_length=250, null=True)
    status =models.SmallIntegerField(null=True)
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.IntegerField(null=True)
    
    
    class Meta:
        db_table="sih_prd_package_type"
        
        
        