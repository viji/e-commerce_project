from django import forms
from django.core.exceptions import ValidationError
from product.models import Product
from product.models import ProductCodes
from product.models import ProductImage
import re

class ProductForm(forms.Form):

    product_group_id =forms.IntegerField(error_messages={'required': 'Empty or invalid Product Id'})
    category_id =forms.IntegerField(error_messages={'required': 'Empty or invalid Category Id'})
    pack_id =forms.IntegerField(error_messages={'required': 'Empty or invalid Pack Id'})
    prt_name = forms.CharField(error_messages={'required': 'Empty or invalid Product Name'})
    seo_name = forms.CharField(error_messages={'required': 'Empty or invalid Seo Name'})
    mat_code = forms.CharField(error_messages={'required': 'Empty or invalid Mat Code'})
    weight =forms.IntegerField(error_messages={'required': 'Empty or invalid Weight'})
    offer_price =forms.FloatField(required=False)
    actual_price =forms.FloatField(error_messages={'required': 'Empty or invalid Actual Price'})
    gtin =forms.IntegerField(required=False)
    description =forms.CharField(required=False)
    prt_status =forms.IntegerField(required=False)
    #cleaned_data = super().clean()
    
    def clean_prt_name(self):
        pn = super().clean()
        prt_name = pn.get('prt_name')
        prt_val=Product.objects.filter(name=prt_name)
        if not prt_val:   
            return True;
        else:  
            msg = "Product Name already exists"
            self.add_error('prt_name', msg)
    def clean_seo_name(self):
        sn = super().clean() 
        seo_name_val= sn.get('seo_name')
        seo_val=Product.objects.filter(name=seo_name_val)
        if not seo_val:   
            return True;
        else:  
            msg = "Seo Name already exists"
            self.add_error('seo_name', msg)


class ProductEditForm(ProductForm):
    product_id =forms.CharField(required=False)
    prt_name = forms.CharField(error_messages={'required': 'Empty or invalid Product Name'})
    seo_name = forms.CharField(error_messages={'required': 'Empty or invalid Seo Name'})

    #cleaned_data = super().clean()
    def clean_prt_name(self):
        pn = super().clean() 
        prt_name_val = pn.get('prt_name')
        return prt_name_val
    def clean_seo_name(self):
        pn = super().clean() 
        seo_name_val = pn.get('seo_name')
        return seo_name_val
    def clean(self):
        pn = super().clean() 
        prt_name_val = self.clean_prt_name()
        print(prt_name_val)
        prt_id = pn.get('product_id')
        print(prt_id)
        prt_val=Product.objects.filter(name=prt_name_val).exclude(id=prt_id)

        if not prt_val:   
            return True;
        else:  
            msg = "Product Name already exists"
            self.add_error('prt_name', msg)
        seo_name_val= self.clean_seo_name()
        #prt_id = pn.get('product_id')
        #print(sn)
        #print(prt_id)
        seo_val=Product.objects.filter(name=seo_name_val).exclude(id=prt_id)
        if not seo_val:   
            return True;
        else:  
            msg = "Seo Name already exists"
            self.add_error('seo_name', msg)

class UploadsForm(forms.Form):
    image  = forms.FileField()
    #product_id =forms.CharField(required=False)
    
    class Meta:  
        model = ProductImage  
        #fields = "__all__"
        fields = ['image']
        