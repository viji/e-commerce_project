from django.shortcuts import render, redirect  
from datetime import date, timedelta
from django.http import HttpResponseRedirect 
from django.http import HttpResponse
from django.conf import settings
import os.path
from category.models import Category
from product_group.models import ProductGroup
from common.views import admin_required
from product.models import ProductPackageType
from product.models import Product
from product.models import ProductCodes
from product.models import ProductImage
from product.forms import ProductForm
from product.forms import UploadsForm
from product.forms import ProductEditForm
from datetime import date, timedelta
import datetime
from django.contrib import messages
from user.models import UserProfile
from django.db.models.functions import Concat
import math
from django.core.files.storage import FileSystemStorage


@admin_required
def product_add(request):
	gtin=0
	cat_values=Category.objects.all()
	prt_values=ProductGroup.objects.all()
	pack_values=ProductPackageType.objects.all()
	if request.method=='POST':
		form = ProductForm(request.POST)
		print(form.errors)
		if form.is_valid():
			if request.POST.get('category_id'):
				category_id = request.POST.get('category_id')
			if request.POST.get('product_group_id'):
			    product_group_id = request.POST.get('product_group_id')
			if request.POST.get('pack_id'):
			    pack_id = request.POST.get('pack_id')
			if request.POST.get('prt_name'):
			    prt_name = request.POST.get('prt_name').strip()
			if request.POST.get('seo_name'):
			    seo_name = request.POST.get('seo_name').strip()
			if request.POST.get('mat_code'):
			    mat_code = request.POST.get('mat_code')
			if request.POST.get('weight'):
			    weight = request.POST.get('weight')
			if request.POST.get('actual_price'):
			    actual_price = request.POST.get('actual_price')
			if request.POST.get('offer_price'):
			    offer_price = request.POST.get('offer_price')
			if request.POST.get('description'):
			    description = request.POST.get('description')
			if request.POST.get('gtin'):
			    gtin = request.POST.get('gtin')
			if request.POST.get('prt_status'):
			    prt_status = request.POST.get('prt_status')
			prt_id_val=ProductCodes(id=None)
			prt_id_val.save()
			print(prt_id_val)
			id_last = ProductCodes.objects.latest('id')
			prt_code="PRT000"+str(id_last.id)
			created_on = datetime.datetime.utcnow()
			created_by = request.session['loggedin_user_id']
			modified_on = None
			modified_by = None
			product_values=Product(product_code=prt_code,product_group_id=product_group_id,category_id=category_id,pack_id=pack_id,name=prt_name,seo_name=seo_name,mat_code=mat_code,weight=weight,offer_price=offer_price,actual_price=actual_price,gtin=gtin,description=description,status=prt_status,created_on=datetime.datetime.utcnow(),created_by=request.session['loggedin_user_id'],modified_on=modified_on,modified_by=modified_by)
			product_values.save()
			messages.success(request, 'Product added successfully.')
			return redirect('/product_list')
	else:
		form = ProductForm()

	return render(request,"product_add.html",{'cat_values':cat_values,'prt_values':prt_values,'pack_values':pack_values,'forms':form})

@admin_required
def product_list(request):
	cat_values=Category.objects.all()
	prt_values=ProductGroup.objects.all()
	pack_values=ProductPackageType.objects.all()
	user_pro_details=UserProfile.objects.all()
	sql = where_str =""
	query_data={}
	search_keyword=""
	search_prt_status=""
	search_cat_code=""
	search_product_group_id=""
	search_pack_id=""
	prt_status=""
	product_group_id=pack_id=""

	#filter data set start
	created_by=request.session['loggedin_user_id']
	print(created_by)
	query_data["created_by"] = created_by
	if request.POST.get('search_keyword',''):
		search_keyword=request.POST.get('search_keyword','')
		query_data['keyword__contains'] = search_keyword
	if request.POST.get('search_cat_code',''):
		search_cat_code=request.POST.get('search_cat_code','')
		query_data['category_id'] = search_cat_code
	if request.POST.get('product_group_id'):
		product_group_id = request.POST.get('product_group_id')
		query_data['product_group_id'] = product_group_id
	if request.POST.get('pack_id'):
		pack_id = request.POST.get('pack_id')
		query_data['pack_id'] = pack_id
	if request.POST.get('status',''):
		prt_status=request.POST.get('status','')
		query_data['status'] = prt_status
	#filter data set end    

	# sorting starts
	sort_str=""
	varSortColumn="created_on"
	varSortOrderVal="DESC"
	if request.POST.get('varSortOrder'):
		varSortOrderVal = request.POST.get('varSortOrder')

	if request.POST.get('varSortCol'):
		varSortColumn = request.POST.get('varSortCol')

	if varSortOrderVal == "ASC":
		sort_str=varSortColumn
	elif varSortOrderVal == "DESC":
		sort_str='-'+varSortColumn

	#paginations starts

	varPageNo = settings.PAGE_NO
	rec_count = 5

	if request.POST.get('count'):
		rec_count = request.POST.get('count')

	if request.POST.get('varPageNo'):
		varPageNo = request.POST.get('varPageNo')

	pageNo = int(varPageNo) - int(1)

	start_from = int(pageNo) * int(rec_count)

	pagination = " LIMIT " + str(start_from) + "," + str(rec_count)

	page_cnt=Product.objects.all().annotate(keyword=Concat('name','seo_name','mat_code')).filter(**query_data).order_by(sort_str)
	print(page_cnt)
	# print(page_cnt.query)
	rec_count_value=settings.REC_COUNT


	page_cnt = len(page_cnt)
	no = int(page_cnt)/int(rec_count)

	total_pages = math.ceil(no);
	startRec=1
	endRec=""
	if int(varPageNo) == 1:
		if int(page_cnt) < int(rec_count):
			endRec = page_cnt
		else:
			endRec = rec_count
	else:
		startRec = int(rec_count) * int(pageNo)
		endRec   = int(startRec) + int(rec_count)
	if int(endRec) > int(page_cnt):
		endRec = page_cnt

	startRec += 1

	#paginations ends
	search_val={'search_keyword':search_keyword,
	'search_cat_code':search_cat_code,
	'search_prt_status':prt_status,
	'search_product_group_id':product_group_id,
	'search_pack_id':pack_id,

	}
	print(search_val)
	product_data = Product.objects.all().annotate(keyword=Concat('name','seo_name','mat_code')).filter(**query_data).order_by(sort_str)[int(start_from):start_from+int(rec_count)]
	return render(request,"product_list.html",{'user_pro_details':user_pro_details,'cat_values':cat_values,'prt_values':prt_values,'pack_values':pack_values,'product_data':product_data,'search_val':search_val,'endRec':endRec,'startRec':startRec,'total_pages':range(total_pages),'page_cnt':page_cnt,'varPageNo':int(varPageNo),'total_no':int(total_pages),'rec_count':int(rec_count),'rec_count_value':rec_count_value,'tatal_rec' : page_cnt,'varSortCol':varSortColumn,'varSortOrder':varSortOrderVal,})   

@admin_required
def product_edit(request,id):
	gtin=0
	form=""
	cat_values=Category.objects.all()
	prt_values=ProductGroup.objects.all()
	pack_values=ProductPackageType.objects.all()
	product_details=Product.objects.get(id=id)
	gtin=0
	cat_values=Category.objects.all()
	prt_values=ProductGroup.objects.all()
	pack_values=ProductPackageType.objects.all()
	if request.method=='POST':
		form = ProductEditForm(request.POST)
		print(form.errors)
		if form.is_valid():
			if request.POST.get('category_id'):
				category_id = request.POST.get('category_id')
			if request.POST.get('product_group_id'):
			    product_group_id = request.POST.get('product_group_id')
			if request.POST.get('pack_id'):
			    pack_id = request.POST.get('pack_id')
			if request.POST.get('prt_name'):
			    prt_name = request.POST.get('prt_name').strip()
			if request.POST.get('seo_name'):
			    seo_name = request.POST.get('seo_name').strip()
			if request.POST.get('mat_code'):
			    mat_code = request.POST.get('mat_code')
			if request.POST.get('weight'):
			    weight = request.POST.get('weight')
			if request.POST.get('actual_price'):
			    actual_price = request.POST.get('actual_price')
			if request.POST.get('offer_price'):
			    offer_price = request.POST.get('offer_price')
			if request.POST.get('description'):
			    description = request.POST.get('description')
			if request.POST.get('gtin'):
			    gtin = request.POST.get('gtin')
			if request.POST.get('prt_status'):
			    prt_status = request.POST.get('prt_status')
			modified_on = datetime.datetime.utcnow()
			modified_by = request.session['loggedin_user_id']
			product_values=Product.objects.filter(id=id).update(product_group_id=product_group_id,category_id=category_id,pack_id=pack_id,name=prt_name,seo_name=seo_name,mat_code=mat_code,weight=weight,offer_price=offer_price,actual_price=actual_price,gtin=gtin,description=description,status=prt_status,modified_on=modified_on,modified_by=modified_by)
			messages.success(request, 'Product Updated successfully.')
			return redirect('/product_list')

	return render(request,"product_edit.html",{'cat_values':cat_values,'prt_values':prt_values,'pack_values':pack_values,'product_details':product_details,'product_id':id,'forms':form})

@admin_required
def product_view(request,id):
	product_values=Product.objects.get(id=id)
	cat_values=Category.objects.all()
	prt_values=ProductGroup.objects.all()
	pack_values=ProductPackageType.objects.all()
	user_pro_details=UserProfile.objects.all()
	context = {}
	image_error=""
	default=""
	img_query_list=[]
	if request.method == 'POST':
		form = UploadsForm(request.POST,request.FILES)
		
		if form.is_valid():
			print(len(request.FILES.getlist('image')))
			for f in request.FILES.getlist('image'):
				
				fs = FileSystemStorage()
				name = fs.save(f.name, f)
				context['url'] = fs.url(name)
				file_name_data = context['url'].replace("%20", " ")
				#img.append(file_name_data)
				image_form=ProductImage(product_id=id,image=file_name_data,is_default=2)
				img_query_list.append(image_form)
			if img_query_list:
				ProductImage.objects.bulk_create(img_query_list)
				messages.success(request, 'Product image added successfully.')
				return redirect('/product_view/'+str(id))
	#print(img)
	images=ProductImage.objects.filter(product_id=id)

	return render(request,"product_view.html",{'product_values':product_values,'cat_values':cat_values,'prt_values':prt_values,'pack_values':pack_values,'user_pro_details':user_pro_details,"image_error":image_error,'images':images})   

@admin_required
def product_default(request,id):
	product_img=ProductImage.objects.get(id=id)
	ProductImage.objects.filter(id=id).update(is_default=1)
	ProductImage.objects.filter(product_id=product_img.product_id).exclude(id=id).update(is_default=2)
	messages.success(request, 'Default image successfully Updated.')
	return redirect('/product_view/'+str(product_img.product_id))

@admin_required
def product_delete(request,id):
	ProductImage.objects.filter(product_id=id).delete()
	Product.objects.filter(id=id).delete()
	
	messages.success(request, 'Product Deleted successfully.')
	return redirect('/product_list')
