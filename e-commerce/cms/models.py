from django.db import models

class Cms(models.Model):
    
    name = models.CharField(max_length=250, null=True)
    content = models.TextField(null=True)
    status =models.SmallIntegerField(null=True)
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.IntegerField(null=True)
    
    class Meta:
        db_table="sih_cms"
        