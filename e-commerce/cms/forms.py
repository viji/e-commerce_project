from django import forms
from django.core.exceptions import ValidationError
from cms.models import Cms
import re

class CmsForm(forms.Form):

    cms_name = forms.CharField(error_messages={'required': 'Empty or invalid Cms Name'})
    cms_content = forms.CharField(required=False)
    cms_status = forms.IntegerField(required=False)
    #cleaned_data = super().clean()
    
    def clean_cms_name(self):
        cn = super().clean() 
        cms_name = cn.get('cms_name')
        cms_val=Cms.objects.filter(name=cms_name)
        if not cms_val:   
            return True;
        else:  
            msg = "Cms Name already exists"
            self.add_error('cms_name', msg)

class CmsEditForm(forms.Form):

    cms_name = forms.CharField(error_messages={'required': 'Empty or invalid Cms Name'})
    cms_content = forms.CharField(required=False)
    cms_status = forms.IntegerField(required=False)
    cms_id = forms.CharField(required=False)
    #cleaned_data = super().clean()
    def clean(self):
        cn = super().clean()
        cms_name = cn.get('cms_name')
        cms_id = cn.get('cms_id')
        print(cms_id)
        cms_val=Cms.objects.filter(name=cms_name).exclude(id=cms_id)
        if not cms_val:   
            return True;
        else:  
            msg = "Cms Name already exists"
            self.add_error('cms_name', msg)