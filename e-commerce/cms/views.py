from django.shortcuts import render, redirect  
from datetime import date, timedelta
from django.http import HttpResponseRedirect 
from django.http import HttpResponse
from django.conf import settings
import os.path
from django.db.models.functions import Concat
from common.views import admin_required
import datetime
from django.contrib import messages
from cms.forms import CmsForm
from cms.forms import CmsEditForm
from cms.models import Cms
import math
from user.models import UserProfile

@admin_required
def cms_add(request):
	forms=cms_content=""
	if request.method=='POST':
		forms = CmsForm(request.POST)
		
		if forms.is_valid():
			if request.POST.get('cms_name'):
			    cms_name = request.POST.get('cms_name').strip()
			if request.POST.get('cms_content'):
			    cms_content = request.POST.get('cms_content')
			if request.POST.get('cms_status'):
			    cms_status = request.POST.get('cms_status')
			created_on = datetime.datetime.utcnow()
			created_by = request.session['loggedin_user_id']
			modified_on = None
			modified_by = None
			cms_values=Cms(name=cms_name,content=cms_content,status=cms_status,created_on=created_on,created_by=created_by,modified_on=modified_on,modified_by=modified_by)
			cms_values.save()
			messages.success(request, 'Cms Added Successfully.')
			return redirect('/cms_list')
	else:
		forms = CmsForm()
	return render(request,"cms_add.html",{'forms':forms})

@admin_required
def cms_list(request):
	sql = where_str =""
	query_data={}
	search_keyword=""
	search_status=0
	user_pro_details=UserProfile.objects.all()

	#filter data set start
	created_by=request.session['loggedin_user_id']
	
	query_data["created_by"] = created_by
	if request.POST.get('search_keyword',''):
		search_keyword=request.POST.get('search_keyword','')
		query_data['keyword__contains'] = search_keyword
	if request.POST.get('status'):
		search_status=request.POST.get('status')
		query_data["status"] = search_status
	#filter data set end    

	# sorting starts
	sort_str=""
	varSortColumn="created_on"
	varSortOrderVal="DESC"
	if request.POST.get('varSortOrder'):
		varSortOrderVal = request.POST.get('varSortOrder')

	if request.POST.get('varSortCol'):
		varSortColumn = request.POST.get('varSortCol')

	if varSortOrderVal == "ASC":
		sort_str=varSortColumn
	elif varSortOrderVal == "DESC":
		sort_str='-'+varSortColumn

	#paginations starts

	varPageNo = settings.PAGE_NO
	rec_count = 5

	if request.POST.get('count'):
		rec_count = request.POST.get('count')

	if request.POST.get('varPageNo'):
		varPageNo = request.POST.get('varPageNo')

	pageNo = int(varPageNo) - int(1)

	start_from = int(pageNo) * int(rec_count)

	pagination = " LIMIT " + str(start_from) + "," + str(rec_count)

	page_cnt=Cms.objects.all().annotate(keyword=Concat('name','name')).filter(**query_data).order_by(sort_str)
	# print(page_cnt.query)
	rec_count_value=settings.REC_COUNT


	page_cnt = len(page_cnt)
	no = int(page_cnt)/int(rec_count)

	total_pages = math.ceil(no);
	startRec=1
	endRec=""
	if int(varPageNo) == 1:
		if int(page_cnt) < int(rec_count):
			endRec = page_cnt
		else:
			endRec = rec_count
	else:
		startRec = int(rec_count) * int(pageNo)
		endRec   = int(startRec) + int(rec_count)
	if int(endRec) > int(page_cnt):
		endRec = page_cnt

		startRec += 1

	#paginations ends
	search_val={
	'search_keyword':search_keyword,
	'search_status':search_status

	}
	cms_data = Cms.objects.all().annotate(keyword=Concat('name','name')).filter(**query_data).order_by(sort_str)[int(start_from):start_from+int(rec_count)]
    
    
	return render(request,"cms_list.html",{'cms_data':cms_data,'search_val':search_val,'endRec':endRec,'startRec':startRec,'total_pages':range(total_pages),'page_cnt':page_cnt,'varPageNo':int(varPageNo),'total_no':int(total_pages),'rec_count':int(rec_count),'rec_count_value':rec_count_value,'tatal_rec' : page_cnt,'varSortCol':varSortColumn,'varSortOrder':varSortOrderVal,'user_pro_details':user_pro_details})

@admin_required
def cms_edit(request,id):
	forms=""
	cms_values=Cms.objects.get(id=id)
	if request.method=='POST':
		forms = CmsEditForm(request.POST)
		print(forms.errors)
		if forms.is_valid():
			if request.POST.get('cms_name'):
				cms_name = request.POST.get('cms_name').strip()
			if request.POST.get('cms_content'):
				cms_content = request.POST.get('cms_content')
			if request.POST.get('cms_status'):
				cms_status = request.POST.get('cms_status')
			created_on = None
			created_by = None
			modified_on = datetime.datetime.utcnow()
			modified_by = request.session['loggedin_user_id']
			cms_values=Cms.objects.filter(id=id).update(name=cms_name,content=cms_content,status=cms_status,modified_on=modified_on,modified_by=modified_by)
			messages.success(request, 'Cms Updated Successfully.')
			return redirect('/cms_list')
	return render(request,"cms_edit.html",{'cms_values':cms_values,'cms_id':id,'forms':forms})   

@admin_required
def cms_view(request,id):

	cms_values=Cms.objects.get(id=id)
	user_pro_details=UserProfile.objects.all()
	return render(request,"cms_view.html",{'cms_values':cms_values,'user_pro_details':user_pro_details})   

@admin_required
def cms_delete(request,id):

	cms_values=Cms.objects.filter(id=id).delete()
	messages.success(request, 'Cms Deleted Successfully.')
	return redirect('/cms_list')