
from django.shortcuts import render
from django.http import HttpResponseRedirect 
import json
from django.http import JsonResponse

from datetime import date, timedelta
import datetime

from product.models import Product
from product.models import ProductPackageType
from product_group.models import ProductGroup
from product.models import ProductImage
from cart.models import Cart
from cart.models import Subscription

def admin_required(function):
    def wrapper(request, *args, **kw):
        if 'loggedin_user_type' in request.session:
            if (request.session.get('loggedin_user_type') != "EMPLOYEE" ):
                return HttpResponseRedirect('/')
            else:
                return function(request, *args, **kw)
        else:
            return HttpResponseRedirect('/')
    return wrapper  
    
def user_login_required(function):
   
    def wrapper(request, *args, **kw):
        if 'loggedin_user_type' in request.session:
           
            if (request.session.get('loggedin_user_type') == "EMPLOYEE" ):
                return function(request, *args, **kw)
            elif (request.session.get('loggedin_user_type') == "CUSTOMER" ):
                return function(request, *args, **kw)
            elif (request.session.get('loggedin_user_type') == "DEALER" ):
                return function(request, *args, **kw)
            else:
                return HttpResponseRedirect('/user_login')
                
        else:
            return HttpResponseRedirect('/user_login')
    return wrapper  
    
def home(request):
    return HttpResponseRedirect('/products_list/all')
  
def load_products_list(request, code=""):
    if code == "all":
        products = Product.objects.filter(status=1)
    else:
        products = Product.objects.filter(product_group_id=code,status=1)
    product_group = ProductGroup.objects.filter(status=1)
    product_image = ProductImage.objects.filter(is_default=1)
	
    return render(request,"products_list.html",{'product_group':product_group,'products':products,'code':code,'product_image':product_image})
    
def load_product(request, code):
    product_info = Product.objects.get(seo_name=code)
    # product_group = ProductGroup.objects.filter(id=product_info.product_group_id,status=1)
    package_type=ProductPackageType.objects.get(id=product_info.pack_id)
    
    related_products= Product.objects.filter(product_group_id=product_info.product_group_id).exclude(id=product_info.id)
    
    return render(request,"product.html",{'product_info':product_info,'code':code,'package_type':package_type,'related_products':related_products})   

def add_to_cart(request):
    response_data={}
    cart_value=""
    if request.method == "POST":
        if request.POST.get('product_id'):
            product_id = request.POST.get('product_id','')
        if request.POST.get('quantity'):
            quantity = request.POST.get('quantity','')
        if request.POST.get('subscription'):
            subscription = request.POST.get('subscription','')
            print(type(subscription))
        if "loggedin_user_id" in request.session:
            user_id=request.session['loggedin_user_id']
            sessions_id=""
        else:
            user_id=None
            sessions_id = request.COOKIES['csrftoken']
            
        if user_id != None:
            cart_value= Cart.objects.filter(product_id=product_id,user_id=user_id)
        else:
            cart_value = Cart.objects.filter(product_id=product_id,sessions_id=sessions_id)
        if cart_value:
            response_data['result'] = 'Exists'
        else:
            print(product_id)
            Cart(user_id=user_id,product_id=product_id,quantity=quantity,subscription=subscription,sessions_id=sessions_id,created_on= datetime.datetime.utcnow()).save()
            Subscription(product_id=product_id,subscription_month=subscription,created_on= datetime.datetime.utcnow()).save()

            response_data['result'] = 'Success'
        
            

    return JsonResponse(response_data) 
  
def get_cart_data_count(request):
    response_data={}
    cart_info = ""
    if "loggedin_user_id" in request.session:
        user_id=request.session['loggedin_user_id']
        cart_info = Cart.objects.filter(user_id=user_id)
    else:
        sessions_id = request.COOKIES['csrftoken']
        cart_info = Cart.objects.filter(sessions_id=sessions_id)
    
    response_data['count'] = len(cart_info)
  
    return JsonResponse(response_data) 
   
def load_wishlist(request):

    return render(request,"wishlist.html")
    
def load_cart(request):

    return render(request,"cart.html")
    
def load_checkout(request):

    return render(request,"checkout.html")
    
def load_about(request):

    return render(request,"about.html")
       
def load_blog(request):

    return render(request,"blog.html")
    
       
def load_contact(request):

    return render(request,"contact.html")

