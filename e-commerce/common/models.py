from django.db import models

class ShippingMethod(models.Model):
    
    carrier = models.CharField(max_length=100, null=True)
    method = models.CharField(max_length=100, null=True)
    status = models.SmallIntegerField(null=True)    
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.IntegerField(null=True)
    
    class Meta:
        db_table="sih_shipping_method"
        
class ShipWeightCost(models.Model):
    
    min_weight = models.FloatField(null=True)
    max_weight = models.FloatField(null=True)
    exc_vat = models.FloatField(null=True)
    inc_vat = models.FloatField(null=True)
  
    
    class Meta:
        db_table="sih_ship_weight_cost"
              
class Weights(models.Model):
    
    measurement = models.CharField(max_length=100, null=True)
    weight = models.FloatField(null=True)
    status = models.SmallIntegerField(null=True)    
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.IntegerField(null=True)
  
    
    class Meta:
        db_table="sih_weights"
        