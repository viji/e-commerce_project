from django import forms
from django.core.exceptions import ValidationError
from category.models import Category
from category.models import CategoryCodes
import re

class CategoryForm(forms.Form):

    parent_id = forms.CharField(required=False)
    cat_name = forms.CharField(error_messages={'required': 'Empty or invalid Name'})
    cat_status = forms.IntegerField(required=False)
    #cleaned_data = super().clean()
    
    def clean_cat_name(self):
        cn = super().clean() 
        cat_name = cn.get('cat_name')
        cat_val=Category.objects.filter(cat_name=cat_name)
        if not cat_val:   
            return True;
        else:  
            msg = "Name already exists"
            self.add_error('cat_name', msg)

class CategoryEditForm(forms.Form):

    parent_id = forms.CharField(required=False)
    cat_name = forms.CharField(error_messages={'required': 'Empty or invalid Name'})
    cat_status = forms.IntegerField(required=False)
    cat_id = forms.CharField(required=False)
    #cleaned_data = super().clean()
    def clean(self):
        cn = super().clean() 
        cat_name = cn.get('cat_name')
        #pat_id = cn.get('parent_id')
        cat_id_val = cn.get('cat_id')
        print(cat_id_val)
        print(cat_name)
        cat_val=Category.objects.filter(cat_name=cat_name).exclude(id=cat_id_val)
        if cat_val:
            msg = "Name already exists"
            self.add_error('cat_name', msg)   
            
        else:  
            return True;