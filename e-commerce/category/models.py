from django.db import models


class Category(models.Model):
    
    parent_id = models.IntegerField(null=True)   
    cat_code = models.CharField(max_length=250, null=True)
    cat_name = models.CharField(max_length=250, null=True)
    status =models.SmallIntegerField(null=True)
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.IntegerField(null=True)
    
    class Meta:
        db_table="sih_category"
        
class CategoryCodes(models.Model):
    
    class Meta:
        db_table ="sih_category_codes"