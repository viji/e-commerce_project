from django.shortcuts import render, redirect  
from datetime import date, timedelta
from django.http import HttpResponseRedirect 
from django.http import HttpResponse
from django.conf import settings
import os.path
from category.models import Category
from category.models import CategoryCodes
from datetime import date, timedelta
import datetime
from category.forms import CategoryForm
from category.forms import CategoryEditForm
from django.contrib import messages
from django.db.models.functions import Concat
from user.models import UserProfile
import math
from common.views import admin_required

@admin_required
def category_add(request):
	forms=""
	category_values=Category.objects.all()
	if request.method=='POST':
		forms = CategoryForm(request.POST)
		print(forms.errors)
		if forms.is_valid():
			if request.POST.get('parent_id'):
			    parent_id = request.POST.get('parent_id')
			if request.POST.get('cat_name'):
			    cat_name = request.POST.get('cat_name').strip()
			if request.POST.get('cat_status'):
			    cat_status = request.POST.get('cat_status')
			cat_id_val=CategoryCodes(id=None)
			cat_id_val.save()
			print(cat_id_val)
			cat_id_last = CategoryCodes.objects.latest('id')
			cat_code="CAT000"+str(cat_id_last.id)
			created_on = datetime.datetime.utcnow()
			created_by = request.session['loggedin_user_id']
			modified_on = None
			modified_by = None
			cat_values=Category(parent_id=parent_id,cat_code=cat_code,cat_name=cat_name,status=cat_status,created_on=created_on,created_by=created_by,modified_on=modified_on,modified_by=modified_by)
			cat_values.save()
			messages.success(request, 'Category added successfully.')
			return redirect('/category_list')
	else:
		forms = CategoryForm()	
	return render(request,"category_add.html",{'category_values':category_values,'forms':forms})
 
@admin_required
def category_edit(request,id):
	forms=""
	cat_values=Category.objects.get(id=id)
	category_values=Category.objects.all().exclude(cat_name=cat_values.cat_name)
	if request.method=='POST':
		forms = CategoryEditForm(request.POST)
		
		if forms.is_valid():
			if request.POST.get('cat_id'):
			   cat_id = request.POST.get('cat_id')
			print(cat_id)
			if request.POST.get('cat_name'):
			    cat_name = request.POST.get('cat_name').strip()
			if request.POST.get('cat_status'):
			    cat_status = request.POST.get('cat_status')
			modified_on = datetime.datetime.utcnow()
			cat_values=Category.objects.filter(id=id).update(cat_name=cat_name,status=cat_status,modified_on=modified_on,modified_by=request.session['loggedin_user_id'])
			messages.success(request, 'Category Updated successfully.')
			return redirect('/category_list')
	return render(request,"category_edit.html",{'cat_values':cat_values,'forms':forms,'category_values':category_values,'cat_id':id})

@admin_required
def category_list(request):
	user_pro_details=UserProfile.objects.all()
	cat_values=Category.objects.all()
	sql = where_str =""
	query_data={}
	search_keyword=""
	search_cat_code=""
	search_cat_status=""

	#filter data set start
	created_by=request.session['loggedin_user_id']
	
	query_data["created_by"] = created_by
	if request.POST.get('search_keyword',''):
		search_keyword=request.POST.get('search_keyword','')
		query_data['keyword__contains'] = search_keyword
	if request.POST.get('search_cat_code',''):
		search_cat_code=request.POST.get('search_cat_code','')
		query_data['id'] = search_cat_code
	if request.POST.get('cat_status',''):
		cat_status=request.POST.get('cat_status','')
		query_data['status'] = cat_status
	#filter data set end    

	# sorting starts
	sort_str=""
	varSortColumn="created_on"
	varSortOrderVal="DESC"
	if request.POST.get('varSortOrder'):
		varSortOrderVal = request.POST.get('varSortOrder')

	if request.POST.get('varSortCol'):
		varSortColumn = request.POST.get('varSortCol')

	if varSortOrderVal == "ASC":
		sort_str=varSortColumn
	elif varSortOrderVal == "DESC":
		sort_str='-'+varSortColumn

	#paginations starts

	varPageNo = settings.PAGE_NO
	rec_count = 5

	if request.POST.get('count'):
		rec_count = request.POST.get('count')

	if request.POST.get('varPageNo'):
		varPageNo = request.POST.get('varPageNo')

	pageNo = int(varPageNo) - int(1)

	start_from = int(pageNo) * int(rec_count)

	pagination = " LIMIT " + str(start_from) + "," + str(rec_count)

	page_cnt=Category.objects.all().annotate(keyword=Concat('cat_name','cat_code')).filter(**query_data).order_by(sort_str)
	# print(page_cnt.query)
	rec_count_value=settings.REC_COUNT


	page_cnt = len(page_cnt)
	no = int(page_cnt)/int(rec_count)

	total_pages = math.ceil(no);
	startRec=1
	endRec=""
	if int(varPageNo) == 1:
		if int(page_cnt) < int(rec_count):
			endRec = page_cnt
		else:
			endRec = rec_count
	else:
		startRec = int(rec_count) * int(pageNo)
		endRec   = int(startRec) + int(rec_count)
	if int(endRec) > int(page_cnt):
		endRec = page_cnt
		startRec += 1

	#paginations ends
	search_val={
	'search_keyword':search_keyword,
	'search_cat_code':search_cat_code,
	'search_cat_status':search_cat_status

	}
	cat_data = Category.objects.all().annotate(keyword=Concat('cat_name','cat_code')).filter(**query_data).order_by(sort_str)[int(start_from):start_from+int(rec_count)]
	category_data=Category.objects.all()
	return render(request,"category_list.html",{'cat_values':cat_values,'cat_data':cat_data,'search_val':search_val,'endRec':endRec,'startRec':startRec,'total_pages':range(total_pages),'page_cnt':page_cnt,'varPageNo':int(varPageNo),'total_no':int(total_pages),'rec_count':int(rec_count),'rec_count_value':rec_count_value,'tatal_rec' : page_cnt,'varSortCol':varSortColumn,'varSortOrder':varSortOrderVal,'user_pro_details':user_pro_details,'category_data':category_data})

@admin_required
def category_view(request,id):
	cat_values=Category.objects.get(id=id)
	return render(request,"category_view.html",{'cat_values':cat_values})
  