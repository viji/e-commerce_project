"""south_indian_hub URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from common import views as common
from user import views as user
from category import views as category
from cms import views as cms
from coupon import views as coupon
from product import views as product
from product_group import views as product_group
from frontend import views as frontend

from address import views as address
from cart import views as cart
from orders import views as orders

urlpatterns = [
    path('admin/', admin.site.urls),
    
    path('user_login/', user.user_login,name='user_login'),
    path('user_register/', user.user_register,name='user_register'),
    path('logout/', user.user_logout,name='user_logout'),
    
    path('user_profile/<int:id>',user.user_profile,name='user_profile'),
    path('user_profile_edit/<int:id>',user.user_profile_edit,name='user_profile_edit'),
    
    path('user_add/',user.user_add,name='user_add'),
    path('user_edit/<int:id>',user.user_edit,name='user_edit'),
    path('user_list/',user.user_list,name='user_list'),
    path('user_view/<int:id>',user.user_view,name='user_view'),
    path('forgot_password/', user.forgot_password),
    
    path('', common.home),
    path('home/', common.home),
    path('products_list/<str:code>', common.load_products_list),
    path('product/<str:code>', common.load_product),
    path('add_to_cart/', common.add_to_cart),
    path('get_cart_data_count/', common.get_cart_data_count),
   
    
    path('cart/', cart.load_cart),
    path('item_update', cart.item_update),
    path('subscription_update', cart.subscription_update),
    path('subscription_list', orders.subscription_list),
    path('item_remove/<int:id>', cart.item_remove),
    path('get_user_data/', cart.get_user_data),
    path('confirm_address/<int:address_id>', cart.confirm_address),
	
    path('order_confirmation/', orders.order_confirmation),
    path('create_order/', orders.order_create),   
    path('order_list/',orders.order_list,name='order_list'), 
    path('order_view/<int:id>',orders.order_view,name='order_view'),
	path('order_status_update/<int:order_id>/<int:status_id>',orders.order_status_update,name='order_status_update'),

    path('wishlist/', common.load_wishlist),
    path('checkout/', common.load_checkout),
    path('about/', common.load_about),
    path('blog/', common.load_blog),
    path('contact/', common.load_contact),
   
    path('category_add/', category.category_add,name='category_add'),
    path('category_list/', category.category_list,name='category_list'),
    path('category_edit/<int:id>', category.category_edit,name='category_edit'),
    path('category_view/<int:id>', category.category_view,name='category_view'),
    
    path('product_group_add/',product_group.product_group_add,name='product_group_add'),
    path('product_group_list/', product_group.product_group_list,name='product_group_list'),
    path('product_group_edit/<int:id>',product_group.product_group_edit,name='product_group_edit'),
    path('product_group_view/<int:id>',product_group.product_group_view,name='product_group_view'),
    path('product_group_img_default/<int:id>',product_group.product_group_img_default,name='product_group_img_default'),
    path('product_group_delete/<int:id>',product_group.product_group_delete,name='product_group_delete'),
	
    path('product_add/',product.product_add,name='product_add'),
    path('product_list/', product.product_list,name='product_list'),
    path('product_edit/<int:id>',product.product_edit,name='product_edit'),
    path('product_view/<int:id>',product.product_view,name='product_view'),
    path('product_delete/<int:id>',product.product_view,name='product_view'),
    path('product_default/<int:id>',product.product_default,name='product_default'),
	
    path('cms_add/',cms.cms_add,name='cms_add'),
    path('cms_edit/<int:id>',cms.cms_edit,name='cms_edit'),
    path('cms_list/', cms.cms_list,name='cms_list'),
    path('cms_view/<int:id>', cms.cms_view,name='cms_view'),
    path('cms_delete/<int:id>', cms.cms_delete,name='cms_delete'),
    
    path('coupon_add/',coupon.coupon_add,name='coupon_add'),
    path('coupon_list/', coupon.coupon_list,name='coupon_list'),
    path('coupon_edit/<int:id>',coupon.coupon_edit,name='coupon_edit'),
    path('coupon_view/<int:id>',coupon.coupon_view,name='coupon_view'),
    path('coupon_delete/<int:id>',coupon.coupon_delete,name='coupon_delete'),
    
    
    path('product_checkout/',frontend.product_checkout,name='product_checkout'), 

    
    path('address_add/',address.address_add,name='address_add'), 
    path('address_list/',address.address_list,name='address_list'), 
    path('address_edit/<int:id>',address.address_edit,name='address_edit'), 
    path('address_view/<int:id>',address.address_view,name='address_view'), 
    path('address_delete/<int:id>',address.address_delete,name='address_delete'), 
    
    
    
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
