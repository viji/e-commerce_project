from django import forms
from django.shortcuts import render, redirect  
from django.core.exceptions import ValidationError
import re

from address.models import Address

class AddressForm(forms.Form):


    user_id = forms.CharField(error_messages={'required': 'Empty or invalid user'})
    edit_address_id = forms.CharField(required=False)
    title = forms.CharField(error_messages={'required': 'Empty or invalid title'})
    address1 = forms.CharField(error_messages={'required': 'Empty or invalid address'})
    address2 = forms.CharField(required=False)
    city = forms.CharField(error_messages={'required': 'Empty or invalid city'})
    state = forms.CharField(error_messages={'required': 'Empty or invalid state'})
    postal_code = forms.CharField(error_messages={'required': 'Empty or invalid postal code'})
    country = forms.CharField(error_messages={'required': 'Empty or invalid country'})
    mobile_number = forms.CharField(error_messages={'required': 'Empty or invalid mobile number'})
    land_mark = forms.CharField(required=False)
    default = forms.CharField(required=False)
    status = forms.CharField(required=False)

    def clean(self):
        cleaned_data = super().clean()
        title = cleaned_data.get("title")
        user_id = cleaned_data.get("user_id")
        mobile_number = cleaned_data.get("mobile_number")
        edit_address_id = cleaned_data.get("edit_address_id")
       
        if mobile_number != "" and mobile_number != None :
            if(mobile_number.isdigit()):
                if len(mobile_number)!=10:
                    msg = "Error mobile number allow 10 digits only"
                    self.add_error('mobile_number', msg)
            else:
                msg = "Invalid mobile number"
                self.add_error('mobile_number', msg)
        if title != "" and title != None :
            if edit_address_id != "":
                if Address.objects.filter(title=title,created_by=user_id).exclude(id=edit_address_id).exists():
                    msg = "Error: Title already exists"
                    self.add_error('title', msg)
            else:
                if Address.objects.filter(title=title,created_by=user_id).exists():
                    msg = "Error: Title already exists"
                    self.add_error('title', msg)
            
def check(email):  
	# for validating an Email 
	regex = '^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$'
	# pass the regular expression 
	# and the string in search() method 
	if(re.search(regex,email)):  
		return 1

	else:
		return 2

def formating_phone_num(number):
    phone_number = ""
    phone_number = number.translate(
                    {
                        ord('('): None,
                        ord(')'): None,
                        ord(','): None,
                        ord('.'): None,
                        ord('-'): None,
                        ord(' '): None,
                    })
    return phone_number;