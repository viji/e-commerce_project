from django.db import models

class Address(models.Model):
    
    title = models.CharField(max_length=250, null=True)
    address1 = models.CharField(max_length=250, null=True)
    address2 = models.CharField(max_length=250, null=True)
    city = models.CharField(max_length=100, null=True)
    state = models.CharField(max_length=100, null=True)
    post_code = models.CharField(max_length=100, null=True)
    country = models.CharField(max_length=100, null=True)
    mobile = models.CharField(max_length=100, null=True)
    land_mark = models.CharField(max_length=250, null=True)
    default =models.SmallIntegerField(null=True)
    status =models.SmallIntegerField(null=True)
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.IntegerField(null=True)
    
    class Meta:
        db_table="sih_address"
        