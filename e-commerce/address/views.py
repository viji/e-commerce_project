from django.shortcuts import render, redirect  
from datetime import date, timedelta
from django.http import HttpResponseRedirect 
from django.http import HttpResponse
from django.conf import settings
from django.contrib import messages
from django.db.models.functions import Concat
import math
import os.path
import datetime
from common.views import user_login_required

from address.models import Address
from address.forms import AddressForm
@user_login_required
def address_add(request):
    form=""
    
    if request.method == "POST":
        form = AddressForm(request.POST) 
        
        if form.is_valid():
            title = request.POST.get('title')
            address1 = request.POST.get('address1')
            address2 = request.POST.get('address2')
            city = request.POST.get('city')
            state = request.POST.get('state')
            postal_code = request.POST.get('postal_code')
            country = request.POST.get('country')
            mobile = request.POST.get('mobile_number')
            land_mark = request.POST.get('land_mark')
            default = request.POST.get('default')
            if default == None or default =="":
                if Address.objects.filter(created_by=request.session['loggedin_user_id']).exists():
                    default = 2
                else:
                    default = 1
                    
            status = request.POST.get('status')
            created_on = datetime.datetime.utcnow()
            created_by = request.session['loggedin_user_id']
            modified_on = None
            modified_by = None

            Address(title=title,address1=address1,address2=address2,city=city,state=state,post_code=postal_code,country=country,mobile=mobile,land_mark=land_mark,default=default,status=status,created_on=created_on,created_by=created_by).save()
            
            messages.success(request, 'Address added successfully.')
            return HttpResponseRedirect("/address_list")
          
    return render(request,"address_add.html",{'form':form,})

@user_login_required   
def address_list(request):
    
    sql = where_str =""
    query_data={}
    search_keyword=""
    
    #filter data set start
    created_by=request.session['loggedin_user_id']
    query_data["created_by"] = created_by
    if request.POST.get('search_keyword',''):
        search_keyword=request.POST.get('search_keyword','')
        query_data['keyword__contains'] = search_keyword
    #filter data set end    
        
    # sorting starts
    sort_str=""
    varSortColumn="created_on"
    varSortOrderVal="DESC"
    if request.POST.get('varSortOrder'):
        varSortOrderVal = request.POST.get('varSortOrder')
    
    if request.POST.get('varSortCol'):
        varSortColumn = request.POST.get('varSortCol')
    
    if varSortOrderVal == "ASC":
        sort_str=varSortColumn
    elif varSortOrderVal == "DESC":
        sort_str='-'+varSortColumn
    
    #paginations starts
    
    varPageNo = settings.PAGE_NO
    rec_count = 5

    if request.POST.get('count'):
        rec_count = request.POST.get('count')
        
    if request.POST.get('varPageNo'):
        varPageNo = request.POST.get('varPageNo')
        
    pageNo = int(varPageNo) - int(1)

    start_from = int(pageNo) * int(rec_count)

    pagination = " LIMIT " + str(start_from) + "," + str(rec_count)

    page_cnt=Address.objects.all().annotate(keyword=Concat('address1','address2','city','country','land_mark', 'mobile','post_code','state','title')).filter(**query_data).order_by(sort_str)
    # print(page_cnt.query)
    rec_count_value=settings.REC_COUNT


    page_cnt = len(page_cnt)
    no = int(page_cnt)/int(rec_count)

    total_pages = math.ceil(no);
    startRec=1
    endRec=""
    if int(varPageNo) == 1:
        if int(page_cnt) < int(rec_count):
            endRec = page_cnt
        else:
            endRec = rec_count
    else:
        startRec = int(rec_count) * int(pageNo)
        endRec   = int(startRec) + int(rec_count)
        if int(endRec) > int(page_cnt):
            endRec = page_cnt
        
        startRec += 1

    #paginations ends
    search_val={'search_keyword':search_keyword}
    address_data = Address.objects.all().annotate(keyword=Concat('address1','address2','city','country','land_mark', 'mobile','post_code','state','title')).filter(**query_data).order_by(sort_str)[int(start_from):start_from+int(rec_count)]
    
    return render(request,"address_list.html",{'address_data':address_data,'search_val':search_val,'endRec':endRec,'startRec':startRec,'total_pages':range(total_pages),'page_cnt':page_cnt,'varPageNo':int(varPageNo),'total_no':int(total_pages),'rec_count':int(rec_count),'rec_count_value':rec_count_value,'tatal_rec' : page_cnt,'varSortCol':varSortColumn,'varSortOrder':varSortOrderVal,})
    
@user_login_required    
def address_view(request, id):
    address_data = Address.objects.get(id=id)
    return render(request,"address_view.html",{'address_data':address_data})

@user_login_required    
def address_edit(request, id):
    form=""
    
    if request.method == "POST":
        form = AddressForm(request.POST) 
        print(form)
        if form.is_valid():
           
            title = request.POST.get('title')
            address1 = request.POST.get('address1')
            address2 = request.POST.get('address2')
            city = request.POST.get('city')
            state = request.POST.get('state')
            postal_code = request.POST.get('postal_code')
            country = request.POST.get('country')
            mobile = request.POST.get('mobile_number')
            land_mark = request.POST.get('land_mark')
            default = request.POST.get('default')
            if default == None or default =="":
                if Address.objects.filter(created_by=request.session['loggedin_user_id']).exists():
                    default = 2
                else:
                    default = 1
                    
            status = request.POST.get('status')
            modified_on = datetime.datetime.utcnow()
            modified_by = request.session['loggedin_user_id']

            Address.objects.filter(id=id).update(title=title,address1=address1,address2=address2,city=city,state=state,post_code=postal_code,country=country,mobile=mobile,land_mark=land_mark,default=default,status=status,modified_on=modified_on,modified_by=modified_by)
            
            messages.success(request, 'Address updated successfully.')
            return HttpResponseRedirect("/address_list")
          
    
    address_data = Address.objects.get(id=id)
    return render(request,"address_edit.html",{'address_data':address_data,'form':form})

@user_login_required    
def address_delete(request, id):
    Address.objects.filter(id=id).delete()
    messages.success(request, 'Address Deleted successfully.')
    return HttpResponseRedirect("/address_list")
