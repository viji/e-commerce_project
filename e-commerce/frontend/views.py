from django.shortcuts import render

def register(request):

    return render(request,"register.html")
  
def login(request):

    return render(request,"login.html")

def address_add(request):

    return render(request,"address_add.html")
    
def address_list(request):

    return render(request,"address_list.html")
    
def address_view(request):

    return render(request,"address_view.html")
    
def address_edit(request):

    return render(request,"address_edit.html")

def customer_profile_view(request):

    return render(request,"customer_profile_view.html")

def profile_edit(request):

    return render(request,"profile_edit.html")

def product_checkout(request):

    return render(request,"product_checkout.html")

def order_confirmation(request):

    return render(request,"order_confirmation.html")

def order_list(request):

    return render(request,"order_list.html")

def order_view(request):

    return render(request,"order_view.html")


  
