from django.shortcuts import render, redirect  
from datetime import date, timedelta
from django.http import HttpResponseRedirect 
from django.http import HttpResponse
from django.conf import settings
from django.db.models.functions import Concat
import os.path
from coupon.models import Coupon
from coupon.models import CouponCodes
from coupon.forms import CouponForm
from coupon.forms import CouponEditForm
from common.views import admin_required
from datetime import date, timedelta
import datetime
from django.contrib import messages
import math
from user.models import UserProfile
@admin_required
def coupon_add(request):
	form=""
	if request.method=='POST':
		form = CouponForm(request.POST)
		
		if form.is_valid():
			if request.POST.get('coupon_title'):
				coupon_title = request.POST.get('coupon_title').strip()
			if request.POST.get('start_date'):
				start_date = datetime.datetime.strptime(request.POST.get('start_date','') , '%m/%d/%Y').strftime('%Y-%m-%d')
			if request.POST.get('end_date'):
				end_date = datetime.datetime.strptime(request.POST.get('end_date','') , '%m/%d/%Y').strftime('%Y-%m-%d')
			if request.POST.get('cust_frequency'):
				cust_frequency = request.POST.get('cust_frequency')
			if request.POST.get('offer_percentage'):
				offer_percentage = request.POST.get('offer_percentage')
			if request.POST.get('min_amount'):
				min_amount = request.POST.get('min_amount')
			if request.POST.get('coupon_status'):
				coupon_status = request.POST.get('coupon_status')
			coupon_id_val=CouponCodes(id=None)
			coupon_id_val.save()
			
			id_last = CouponCodes.objects.latest('id')
			coupon_code="CPN000"+str(id_last.id)
			created_on = datetime.datetime.utcnow()
			created_by = request.session['loggedin_user_id']
			modified_on = None
			modified_by = None
			coupon_values=Coupon(coupon_code=coupon_code,title=coupon_title,start_date=start_date,end_date=end_date,customer_frequency=cust_frequency,offer_percentage=offer_percentage,min_order_amount=min_amount,status=coupon_status,created_on=created_on,created_by=created_by,modified_on=modified_on,modified_by=modified_by)
			coupon_values.save()
			messages.success(request, 'Coupon Added Successfully!')
			return redirect('/coupon_list')
	else:
		form = CouponForm()
	return render(request,"coupon_add.html",{'forms':form})

@admin_required
def coupon_list(request):
	sql = where_str =""
	query_data={}
	search_keyword=""
	search_status=0
	user_pro_details=UserProfile.objects.all()
	search_start_date=search_end_date=""
	#filter data set start
	created_by=request.session['loggedin_user_id']

	query_data["created_by"] = created_by
	if request.POST.get('search_keyword',''):
		search_keyword=request.POST.get('search_keyword','')
		query_data['keyword__contains'] = search_keyword
	if request.POST.get('status'):
		search_status=request.POST.get('status')
		query_data['status'] = search_status
	if request.POST.get('start_date'):
		search_start_date=request.POST.get('start_date')
		query_data['start_date'] = datetime.datetime.strptime(search_start_date , '%m/%d/%Y').strftime('%Y-%m-%d')
	if request.POST.get('end_date'):
		search_end_date=request.POST.get('end_date')
		query_data['end_date'] =  datetime.datetime.strptime(search_end_date , '%m/%d/%Y').strftime('%Y-%m-%d')
	#filter data set end    
	# if search_start_date and search_end_date: 
		# query_data['date__range'] = [search_start_date , search_end_date]
		
	print(query_data)
	# sorting starts
	sort_str=""
	varSortColumn="created_on"
	varSortOrderVal="DESC"
	if request.POST.get('varSortOrder'):
		varSortOrderVal = request.POST.get('varSortOrder')

	if request.POST.get('varSortCol'):
		varSortColumn = request.POST.get('varSortCol')

	if varSortOrderVal == "ASC":
		sort_str=varSortColumn
	elif varSortOrderVal == "DESC":
		sort_str='-'+varSortColumn

	#paginations starts

	varPageNo = settings.PAGE_NO
	rec_count = 5

	if request.POST.get('count'):
		rec_count = request.POST.get('count')

	if request.POST.get('varPageNo'):
		varPageNo = request.POST.get('varPageNo')

	pageNo = int(varPageNo) - int(1)

	start_from = int(pageNo) * int(rec_count)

	pagination = " LIMIT " + str(start_from) + "," + str(rec_count)
	page_cnt= Coupon.objects.all().annotate(keyword=Concat('title','coupon_code')).filter(**query_data).order_by(sort_str)
	# print(page_cnt.query)
	rec_count_value=settings.REC_COUNT


	page_cnt = len(page_cnt)
	no = int(page_cnt)/int(rec_count)

	total_pages = math.ceil(no);
	startRec=1
	endRec=""
	if int(varPageNo) == 1:
		if int(page_cnt) < int(rec_count):
			endRec = page_cnt
		else:
			endRec = rec_count
	else:
		startRec = int(rec_count) * int(pageNo)
		endRec   = int(startRec) + int(rec_count)
	if int(endRec) > int(page_cnt):
		endRec = page_cnt

		startRec += 1

	#paginations ends
	search_val={
	'search_keyword':search_keyword,
	'search_status':search_status,
	'search_start_date':search_start_date,
	'search_end_date':search_end_date,

	}
	coupon_info=Coupon.objects.all().annotate(keyword=Concat('title','coupon_code')).filter(**query_data).order_by(sort_str)[int(start_from):start_from+int(rec_count)]


	return render(request,"coupon_list.html",{'coupon_info':coupon_info,'search_val':search_val,'endRec':endRec,'startRec':startRec,'total_pages':range(total_pages),'page_cnt':page_cnt,'varPageNo':int(varPageNo),'total_no':int(total_pages),'rec_count':int(rec_count),'rec_count_value':rec_count_value,'tatal_rec' : page_cnt,'varSortCol':varSortColumn,'varSortOrder':varSortOrderVal,'user_pro_details':user_pro_details})


@admin_required
def coupon_edit(request,id):
	form=""
	coupon_values=Coupon.objects.get(id=id)
	if request.method=='POST':
		form = CouponEditForm(request.POST) 
		
		if form.is_valid():
			if request.POST.get('coupon_title'):
				coupon_title = request.POST.get('coupon_title').strip()
			if request.POST.get('start_date'):
				start_date = datetime.datetime.strptime(request.POST.get('start_date','') , '%m/%d/%Y').strftime('%Y-%m-%d')
			if request.POST.get('end_date'):
				end_date = datetime.datetime.strptime(request.POST.get('end_date','') , '%m/%d/%Y').strftime('%Y-%m-%d')
			if request.POST.get('cust_frequency'):
				cust_frequency = request.POST.get('cust_frequency')
			if request.POST.get('offer_percentage'):
				offer_percentage = request.POST.get('offer_percentage')
			if request.POST.get('min_amount'):
				min_amount = request.POST.get('min_amount')
			if request.POST.get('coupon_status'):
				coupon_status = request.POST.get('coupon_status')
			modified_on = datetime.datetime.utcnow()
			modified_by = request.session['loggedin_user_id']
			coupon_values=Coupon.objects.filter(id=id).update(title=coupon_title,start_date=start_date,end_date=end_date,customer_frequency=cust_frequency,offer_percentage=offer_percentage,min_order_amount=min_amount,status=coupon_status,modified_on=modified_on,modified_by=modified_by)
			messages.success(request, 'Coupon Updated Successfully.')
			return redirect('/coupon_list')
	return render(request,"coupon_edit.html",{'coupon_values':coupon_values,'forms':form,'coupon_id':id})
    
@admin_required
def coupon_view(request,id):
    coupon_info=Coupon.objects.get(id=id)
    user_pro_details=UserProfile.objects.all()
    return render(request,"coupon_view.html",{'coupon_info':coupon_info,'user_pro_details':user_pro_details})   
    
@admin_required
def coupon_delete(request,id):
	coupon_info=Coupon.objects.filter(id=id).delete()
	messages.success(request, 'Coupon Deleted Successfully.')
	return redirect('/coupon_list')
