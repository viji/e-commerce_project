from django.db import models

class CouponCodes(models.Model): 
    
    class Meta:
        db_table="sih_coupon_codes"
        
class Coupon(models.Model):
    
    coupon_code = models.CharField(max_length=250, null=True)
    title = models.CharField(max_length=250, null=True)
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    customer_frequency =models.IntegerField(null=True)
    offer_percentage =models.IntegerField(null=True)
    min_order_amount =models.FloatField(null=True)
    status =models.SmallIntegerField(null=True)
    created_on = models.DateTimeField(null=True)
    created_by = models.IntegerField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.IntegerField(null=True)
    
    class Meta:
        db_table="sih_coupon"
        