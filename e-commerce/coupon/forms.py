from django import forms
from django.core.exceptions import ValidationError
from coupon.models import Coupon
from coupon.models import CouponCodes
import re
import datetime

class CouponForm(forms.Form):

    coupon_title = forms.CharField(error_messages={'required': 'Empty or invalid Coupon Title'})
    start_date = forms.DateField(error_messages={'required': 'Empty or invalid Start date'})
    end_date = forms.DateField(error_messages={'required': 'Empty or invalid End date'})
    cust_frequency =forms.IntegerField(error_messages={'required': 'Empty or invalid Customer frequency'})
    offer_percentage =forms.IntegerField(error_messages={'required': 'Empty or invalid Offer percentage'})
    min_amount =forms.FloatField(error_messages={'required': 'Empty or invalid Minimum Amount'})
    coupon_status =forms.IntegerField(required=False)

    #cleaned_data = super().clean()
    def clean_coupon_title(self):
        cn = super().clean() 
        coupon_title= cn.get('coupon_title')
        coupon_title_val=Coupon.objects.filter(title=coupon_title)
        if not coupon_title_val:   
            return True;
        else:  
            msg = "Coupon Title already exists"
            self.add_error('coupon_title', msg)

    def  clean(self):
        ed = super().clean() 
        end_date= ed.get('end_date')
        start_date= ed.get('start_date')
        print(start_date)
        start_date_val=datetime.datetime.combine(start_date, datetime.time(0, 0))
        end_date_val=datetime.datetime.combine(end_date, datetime.time(0, 0))
        today_date = datetime.datetime.utcnow()
        date=datetime.datetime.today().strftime('%Y-%m-%d')
        today_val=datetime.datetime.strptime(date, '%Y-%m-%d')
        print(type(end_date_val))
        print(start_date_val)
        if end_date < start_date:
            msg = "Invalid End date"
            self.add_error('end_date', msg)
        else:
            return True
        if start_date_val < today_val:
            msg = "Invalid Start date"
            self.add_error('start_date', msg)
        else:
            return True

    '''def  clean_start_date(self):
        sd = super().clean() 
        start_date= sd.get('start_date')
        print(start_date)
        today_date = datetime.datetime.utcnow()
        date=datetime.datetime.today().strftime('%Y-%m-%d')
        today_val=datetime.datetime.strptime(date, '%Y-%m-%d')
        start_date_val= datetime.datetime.combine(start_date, datetime.time(0, 0))
        print(type(today_val))
        print(type(start_date_val))
        if start_date_val < today_val:
            msg = "Invalid Start date"
            self.add_error('start_date', msg)
        else:
            return True'''

class CouponEditForm(forms.Form):
    coupon_id =forms.CharField(required=False)
    coupon_title = forms.CharField(error_messages={'required': 'Empty or invalid Coupon Title'})
    start_date = forms.DateField(error_messages={'required': 'Empty or invalid Start date'})
    end_date = forms.DateField(error_messages={'required': 'Empty or invalid End date'})
    cust_frequency =forms.IntegerField(error_messages={'required': 'Empty or invalid Customer frequency'})
    offer_percentage =forms.IntegerField(error_messages={'required': 'Empty or invalid Offer percentage'})
    min_amount =forms.FloatField(error_messages={'required': 'Empty or invalid Minimum Amount'})
    coupon_status =forms.IntegerField(required=False)
    def clean_start_date(self):
        pn = super().clean() 
        start_date = pn.get('start_date')
        return start_date
    def clean_end_date(self):
        pn = super().clean() 
        end_date = pn.get('end_date')
        return end_date
    def clean_coupon_title(self):
        pn = super().clean() 
        coupon_title = pn.get('coupon_title')
        return coupon_title

    def clean_coupon_id(self):
        cpn = super().clean() 
        coupon_title= self.clean_coupon_title()
        coupon_id= cpn.get('coupon_id')
        coupon_title_val=Coupon.objects.filter(title=coupon_title).exclude(id=coupon_id)
        if not coupon_title_val:   
            return True;
        else:  
            msg = "Coupon Title already exists"
            self.add_error('coupon_title', msg)
    def clean(self):
        end_date= self.clean_end_date()
        start_date= self.clean_start_date()
        print(start_date)
        if end_date < start_date:
            msg = "Invalid End date"
            self.add_error('end_date', msg)
        else:
            return True


