-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 15, 2021 at 12:09 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e-commerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add user codes', 7, 'add_usercodes'),
(26, 'Can change user codes', 7, 'change_usercodes'),
(27, 'Can delete user codes', 7, 'delete_usercodes'),
(28, 'Can view user codes', 7, 'view_usercodes'),
(29, 'Can add users', 8, 'add_users'),
(30, 'Can change users', 8, 'change_users'),
(31, 'Can delete users', 8, 'delete_users'),
(32, 'Can view users', 8, 'view_users'),
(33, 'Can add users type', 9, 'add_userstype'),
(34, 'Can change users type', 9, 'change_userstype'),
(35, 'Can delete users type', 9, 'delete_userstype'),
(36, 'Can view users type', 9, 'view_userstype'),
(37, 'Can add category', 10, 'add_category'),
(38, 'Can change category', 10, 'change_category'),
(39, 'Can delete category', 10, 'delete_category'),
(40, 'Can view category', 10, 'view_category'),
(41, 'Can add category codes', 11, 'add_categorycodes'),
(42, 'Can change category codes', 11, 'change_categorycodes'),
(43, 'Can delete category codes', 11, 'delete_categorycodes'),
(44, 'Can view category codes', 11, 'view_categorycodes'),
(45, 'Can add cms', 12, 'add_cms'),
(46, 'Can change cms', 12, 'change_cms'),
(47, 'Can delete cms', 12, 'delete_cms'),
(48, 'Can view cms', 12, 'view_cms'),
(49, 'Can add coupon', 13, 'add_coupon'),
(50, 'Can change coupon', 13, 'change_coupon'),
(51, 'Can delete coupon', 13, 'delete_coupon'),
(52, 'Can view coupon', 13, 'view_coupon'),
(53, 'Can add coupon codes', 14, 'add_couponcodes'),
(54, 'Can change coupon codes', 14, 'change_couponcodes'),
(55, 'Can delete coupon codes', 14, 'delete_couponcodes'),
(56, 'Can view coupon codes', 14, 'view_couponcodes'),
(57, 'Can add product', 15, 'add_product'),
(58, 'Can change product', 15, 'change_product'),
(59, 'Can delete product', 15, 'delete_product'),
(60, 'Can view product', 15, 'view_product'),
(61, 'Can add product codes', 16, 'add_productcodes'),
(62, 'Can change product codes', 16, 'change_productcodes'),
(63, 'Can delete product codes', 16, 'delete_productcodes'),
(64, 'Can view product codes', 16, 'view_productcodes'),
(65, 'Can add product image', 17, 'add_productimage'),
(66, 'Can change product image', 17, 'change_productimage'),
(67, 'Can delete product image', 17, 'delete_productimage'),
(68, 'Can view product image', 17, 'view_productimage'),
(69, 'Can add product package type', 18, 'add_productpackagetype'),
(70, 'Can change product package type', 18, 'change_productpackagetype'),
(71, 'Can delete product package type', 18, 'delete_productpackagetype'),
(72, 'Can view product package type', 18, 'view_productpackagetype'),
(73, 'Can add product group', 19, 'add_productgroup'),
(74, 'Can change product group', 19, 'change_productgroup'),
(75, 'Can delete product group', 19, 'delete_productgroup'),
(76, 'Can view product group', 19, 'view_productgroup'),
(77, 'Can add product group codes', 20, 'add_productgroupcodes'),
(78, 'Can change product group codes', 20, 'change_productgroupcodes'),
(79, 'Can delete product group codes', 20, 'delete_productgroupcodes'),
(80, 'Can view product group codes', 20, 'view_productgroupcodes'),
(81, 'Can add product group image', 21, 'add_productgroupimage'),
(82, 'Can change product group image', 21, 'change_productgroupimage'),
(83, 'Can delete product group image', 21, 'delete_productgroupimage'),
(84, 'Can view product group image', 21, 'view_productgroupimage'),
(85, 'Can add user profile', 22, 'add_userprofile'),
(86, 'Can change user profile', 22, 'change_userprofile'),
(87, 'Can delete user profile', 22, 'delete_userprofile'),
(88, 'Can view user profile', 22, 'view_userprofile'),
(89, 'Can add shipping method', 23, 'add_shippingmethod'),
(90, 'Can change shipping method', 23, 'change_shippingmethod'),
(91, 'Can delete shipping method', 23, 'delete_shippingmethod'),
(92, 'Can view shipping method', 23, 'view_shippingmethod'),
(93, 'Can add ship weight cost', 24, 'add_shipweightcost'),
(94, 'Can change ship weight cost', 24, 'change_shipweightcost'),
(95, 'Can delete ship weight cost', 24, 'delete_shipweightcost'),
(96, 'Can view ship weight cost', 24, 'view_shipweightcost'),
(97, 'Can add weights', 25, 'add_weights'),
(98, 'Can change weights', 25, 'change_weights'),
(99, 'Can delete weights', 25, 'delete_weights'),
(100, 'Can view weights', 25, 'view_weights'),
(101, 'Can add address', 26, 'add_address'),
(102, 'Can change address', 26, 'change_address'),
(103, 'Can delete address', 26, 'delete_address'),
(104, 'Can view address', 26, 'view_address'),
(105, 'Can add cart', 27, 'add_cart'),
(106, 'Can change cart', 27, 'change_cart'),
(107, 'Can delete cart', 27, 'delete_cart'),
(108, 'Can view cart', 27, 'view_cart'),
(109, 'Can add order billing address', 28, 'add_orderbillingaddress'),
(110, 'Can change order billing address', 28, 'change_orderbillingaddress'),
(111, 'Can delete order billing address', 28, 'delete_orderbillingaddress'),
(112, 'Can view order billing address', 28, 'view_orderbillingaddress'),
(113, 'Can add order codes', 29, 'add_ordercodes'),
(114, 'Can change order codes', 29, 'change_ordercodes'),
(115, 'Can delete order codes', 29, 'delete_ordercodes'),
(116, 'Can view order codes', 29, 'view_ordercodes'),
(117, 'Can add order coupon', 30, 'add_ordercoupon'),
(118, 'Can change order coupon', 30, 'change_ordercoupon'),
(119, 'Can delete order coupon', 30, 'delete_ordercoupon'),
(120, 'Can view order coupon', 30, 'view_ordercoupon'),
(121, 'Can add order log', 31, 'add_orderlog'),
(122, 'Can change order log', 31, 'change_orderlog'),
(123, 'Can delete order log', 31, 'delete_orderlog'),
(124, 'Can view order log', 31, 'view_orderlog'),
(125, 'Can add order product', 32, 'add_orderproduct'),
(126, 'Can change order product', 32, 'change_orderproduct'),
(127, 'Can delete order product', 32, 'delete_orderproduct'),
(128, 'Can view order product', 32, 'view_orderproduct'),
(129, 'Can add orders', 33, 'add_orders'),
(130, 'Can change orders', 33, 'change_orders'),
(131, 'Can delete orders', 33, 'delete_orders'),
(132, 'Can view orders', 33, 'view_orders'),
(133, 'Can add order shipping address', 34, 'add_ordershippingaddress'),
(134, 'Can change order shipping address', 34, 'change_ordershippingaddress'),
(135, 'Can delete order shipping address', 34, 'delete_ordershippingaddress'),
(136, 'Can view order shipping address', 34, 'view_ordershippingaddress'),
(137, 'Can add order shipping process', 35, 'add_ordershippingprocess'),
(138, 'Can change order shipping process', 35, 'change_ordershippingprocess'),
(139, 'Can delete order shipping process', 35, 'delete_ordershippingprocess'),
(140, 'Can view order shipping process', 35, 'view_ordershippingprocess'),
(141, 'Can add order status', 36, 'add_orderstatus'),
(142, 'Can change order status', 36, 'change_orderstatus'),
(143, 'Can delete order status', 36, 'delete_orderstatus'),
(144, 'Can view order status', 36, 'view_orderstatus'),
(145, 'Can add subscription', 37, 'add_subscription'),
(146, 'Can change subscription', 37, 'change_subscription'),
(147, 'Can delete subscription', 37, 'delete_subscription'),
(148, 'Can view subscription', 37, 'view_subscription');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(26, 'address', 'address'),
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(27, 'cart', 'cart'),
(37, 'cart', 'subscription'),
(10, 'category', 'category'),
(11, 'category', 'categorycodes'),
(12, 'cms', 'cms'),
(23, 'common', 'shippingmethod'),
(24, 'common', 'shipweightcost'),
(25, 'common', 'weights'),
(5, 'contenttypes', 'contenttype'),
(13, 'coupon', 'coupon'),
(14, 'coupon', 'couponcodes'),
(28, 'orders', 'orderbillingaddress'),
(29, 'orders', 'ordercodes'),
(30, 'orders', 'ordercoupon'),
(31, 'orders', 'orderlog'),
(32, 'orders', 'orderproduct'),
(33, 'orders', 'orders'),
(34, 'orders', 'ordershippingaddress'),
(35, 'orders', 'ordershippingprocess'),
(36, 'orders', 'orderstatus'),
(15, 'product', 'product'),
(16, 'product', 'productcodes'),
(17, 'product', 'productimage'),
(18, 'product', 'productpackagetype'),
(19, 'product_group', 'productgroup'),
(20, 'product_group', 'productgroupcodes'),
(21, 'product_group', 'productgroupimage'),
(6, 'sessions', 'session'),
(7, 'user', 'usercodes'),
(22, 'user', 'userprofile'),
(8, 'user', 'users'),
(9, 'user', 'userstype');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2020-10-21 12:38:44.748401'),
(2, 'auth', '0001_initial', '2020-10-21 12:38:47.812658'),
(3, 'admin', '0001_initial', '2020-10-21 12:38:58.083055'),
(4, 'admin', '0002_logentry_remove_auto_add', '2020-10-21 12:39:00.169508'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2020-10-21 12:39:00.290537'),
(6, 'contenttypes', '0002_remove_content_type_name', '2020-10-21 12:39:01.864248'),
(7, 'auth', '0002_alter_permission_name_max_length', '2020-10-21 12:39:02.605023'),
(8, 'auth', '0003_alter_user_email_max_length', '2020-10-21 12:39:02.754989'),
(9, 'auth', '0004_alter_user_username_opts', '2020-10-21 12:39:02.837949'),
(10, 'auth', '0005_alter_user_last_login_null', '2020-10-21 12:39:03.685504'),
(11, 'auth', '0006_require_contenttypes_0002', '2020-10-21 12:39:03.728303'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2020-10-21 12:39:03.795271'),
(13, 'auth', '0008_alter_user_username_max_length', '2020-10-21 12:39:04.130265'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2020-10-21 12:39:04.262482'),
(15, 'auth', '0010_alter_group_name_max_length', '2020-10-21 12:39:04.408260'),
(16, 'auth', '0011_update_proxy_permissions', '2020-10-21 12:39:04.469773'),
(17, 'category', '0001_initial', '2020-10-21 12:39:05.050072'),
(18, 'cms', '0001_initial', '2020-10-21 12:39:05.496952'),
(19, 'coupon', '0001_initial', '2020-10-21 12:39:07.514427'),
(20, 'product', '0001_initial', '2020-10-21 12:39:08.563859'),
(21, 'product_group', '0001_initial', '2020-10-21 12:39:09.334280'),
(22, 'sessions', '0001_initial', '2020-10-21 12:39:09.787054'),
(23, 'user', '0001_initial', '2020-10-21 12:39:11.790505'),
(24, 'user', '0002_users', '2020-10-22 12:49:41.461313'),
(27, 'user', '0003_auto_20201022_2302', '2020-10-22 17:32:51.365647'),
(28, 'user', '0004_auto_20201022_2319', '2020-10-22 17:49:57.039976'),
(29, 'address', '0001_initial', '2020-10-23 07:38:45.692401'),
(30, 'cart', '0001_initial', '2020-10-23 07:38:46.095703'),
(31, 'common', '0001_initial', '2020-10-23 07:38:46.789664'),
(32, 'orders', '0001_initial', '2020-10-23 07:38:50.075406'),
(33, 'auth', '0012_alter_user_first_name_max_length', '2021-05-11 12:43:12.941993'),
(34, 'cart', '0002_subscription', '2021-05-11 12:43:13.017187'),
(35, 'cart', '0003_cart_suscription', '2021-05-14 13:31:57.432143'),
(36, 'cart', '0004_auto_20210514_1458', '2021-05-14 14:58:20.007316'),
(37, 'orders', '0002_orders_subscription', '2021-05-15 07:41:49.127470'),
(38, 'orders', '0003_orderproduct_prd_subscription', '2021-05-15 07:52:10.075288');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('1bhz8n18rqb9saq28riebnun041zbxxv', '.eJxVj90OgyAMhd-l18xplmUZz7B3IBVQ2UAcwhZjePeBzv3ctOk5bb7TGTg6z9DY0Hum-sYCnWEMNfPWowZ6PhYlASG1ekg3Md6hayXQrKmRL1do_CLYtMFQ69_TSFbA4KwI_EuoclEC6InAx0tjRaBHkwBwQSECJBf5bbPuflr6U6q2S9CqzNimSdzBKS63uMh9QL1paSuLfw8pg_mNtY_7d4TdobgOLcQYX7zOW-g:1lhUOh:feWlQlvhMERTBwZToHsKiAuC79uSSHiaW2qw49Ova6k', '2021-05-28 09:46:19.294734'),
('b8iles8isohacta3zywt6t0vv0g0trjy', 'Y2Q4ZDljN2NiOTJiNGQ4ZDRhY2E3MmRkZDI0NzhkZTc0MmFmOTVmODp7ImxvZ2dlZGluX3VzZXJfaWQiOjEsImxvZ2dlZGluX3VzZXJfY29kZSI6IlVTUjAwMDEiLCJsb2dnZWRpbl91c2VyX3R5cGUiOiJFTVBMT1lFRSIsImxvZ2dlZGluX3VzZXJfZW1haWwiOiJhZG1pbkBtYWlsaW5hdG9yLmNvbSIsImxvZ2dlZGluX3VzZXJfbmFtZSI6IkFkbWluIHVzZXIifQ==', '2020-11-05 11:18:52.526789'),
('dshj8kfng97rhb1vmfi20d1ae4rzfkgc', 'Y2Q4ZDljN2NiOTJiNGQ4ZDRhY2E3MmRkZDI0NzhkZTc0MmFmOTVmODp7ImxvZ2dlZGluX3VzZXJfaWQiOjEsImxvZ2dlZGluX3VzZXJfY29kZSI6IlVTUjAwMDEiLCJsb2dnZWRpbl91c2VyX3R5cGUiOiJFTVBMT1lFRSIsImxvZ2dlZGluX3VzZXJfZW1haWwiOiJhZG1pbkBtYWlsaW5hdG9yLmNvbSIsImxvZ2dlZGluX3VzZXJfbmFtZSI6IkFkbWluIHVzZXIifQ==', '2020-11-09 08:07:39.296951'),
('l8jpswkde5crln9r0tact03yu79lfav3', 'e30:1lhr6v:wlz3n96IFDGCd5HHpfXCpGARDoBwTZXepVchkM0vW8Y', '2021-05-29 10:01:29.615702'),
('nv54mcpskon4r9vpgsyfakh1vyjiupx6', 'Y2Q4ZDljN2NiOTJiNGQ4ZDRhY2E3MmRkZDI0NzhkZTc0MmFmOTVmODp7ImxvZ2dlZGluX3VzZXJfaWQiOjEsImxvZ2dlZGluX3VzZXJfY29kZSI6IlVTUjAwMDEiLCJsb2dnZWRpbl91c2VyX3R5cGUiOiJFTVBMT1lFRSIsImxvZ2dlZGluX3VzZXJfZW1haWwiOiJhZG1pbkBtYWlsaW5hdG9yLmNvbSIsImxvZ2dlZGluX3VzZXJfbmFtZSI6IkFkbWluIHVzZXIifQ==', '2020-11-06 14:20:32.263725'),
('oh1sluhprq32cbamro8po0hhtiqjjrty', 'MjAwNTNiMGRmNWJlM2U1NTVkN2RkM2JjYzcwY2I5NTgxYTczMmZiYzp7fQ==', '2020-11-07 10:11:14.630030'),
('ohdtn0c20a26lmywygqy4icfz59iihxa', 'Y2Q4ZDljN2NiOTJiNGQ4ZDRhY2E3MmRkZDI0NzhkZTc0MmFmOTVmODp7ImxvZ2dlZGluX3VzZXJfaWQiOjEsImxvZ2dlZGluX3VzZXJfY29kZSI6IlVTUjAwMDEiLCJsb2dnZWRpbl91c2VyX3R5cGUiOiJFTVBMT1lFRSIsImxvZ2dlZGluX3VzZXJfZW1haWwiOiJhZG1pbkBtYWlsaW5hdG9yLmNvbSIsImxvZ2dlZGluX3VzZXJfbmFtZSI6IkFkbWluIHVzZXIifQ==', '2020-11-04 14:51:57.282457'),
('xpszqglca94va4soal3g59dbkjqs59xf', '.eJx9Ustu2zAQ_BWDZ8WVlMRtfCoQ9NYgQNz0SqxJWtqGD4WkEgiB_71L2nST2PVFBGeGszMU35h2XackWj4G5TlKtmyrT6BwUrEle1w91HXdss90nIZE3z6uft3f_Xg44pUB1CR4wc6q0H9PW7QQnZ8LZ47kFkyy-71Tz5KfAB85GDfayNFuHFu-sTCueXQRyPnmel5XTCqNL8pPXPTgO7JIGAaRT4GJGXCk4KD1-6Pb_YDBOzmKfxOa9EkXsqjYgaNtU7F9xp8g5Uj5BhBPhXqOU15fFXY9DW3qNHazobmDR6FKXBBxBF0wUiXwQyE0kGrs1vBlH-Hicv5n6NiWUq9R00V2nFJ4FcIheMILSCXYKoKf0QalsmJi1QdBSwLrfOxnIXqlYqEFpiJM9MpawIKGCDFlimBQW8jtMzE4onR5Kgt6KE17sEq_wCc3tBKhwMbRorgdzVp5Ipv28up68fXbTV0UGqzkBvwTsZ5ezStMs5QAnWXUP_Q4nOqf8LP93wtO9M_0Uf-Mnuqfif_031kd9c_w2f5Zcbb_9i9XfEZn:1lgRaK:Yvo-KJNr-TEe0mjNrDP6Rs6tsBD0lfy2vwvQzg84bno', '2021-05-25 12:34:00.856536');

-- --------------------------------------------------------

--
-- Table structure for table `sih_address`
--

CREATE TABLE `sih_address` (
  `id` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `address1` varchar(250) DEFAULT NULL,
  `address2` varchar(250) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `post_code` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `land_mark` varchar(250) DEFAULT NULL,
  `default` smallint(6) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime(6) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_address`
--

INSERT INTO `sih_address` (`id`, `title`, `address1`, `address2`, `city`, `state`, `post_code`, `country`, `mobile`, `land_mark`, `default`, `status`, `created_on`, `created_by`, `modified_on`, `modified_by`) VALUES
(1, 'Home', 'Star residency', 'north street', 'chennai', 'tamilnadu', '600012', 'india', '1234567890', 'railway station', 1, 1, '2020-10-23 09:01:14.239745', 2, '2020-10-23 13:35:03.407719', 2),
(2, 'Office', '5th Extension', 'nandanam', 'chennai', 'Tamilnadu', NULL, 'india', NULL, 'ymca', 2, 1, '2020-10-23 09:07:42.357518', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sih_cart`
--

CREATE TABLE `sih_cart` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `sessions_id` varchar(250) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime(6) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `subscription` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_cart`
--

INSERT INTO `sih_cart` (`id`, `user_id`, `product_id`, `quantity`, `sessions_id`, `created_on`, `created_by`, `modified_on`, `modified_by`, `subscription`) VALUES
(1, NULL, 1, 2, 'oh1sluhprq32cbamro8po0hhtiqjjrty', '2020-10-24 11:52:28.397970', NULL, NULL, NULL, 2),
(2, NULL, 1, 1, 'oh1sluhprq32cbamro8po0hhtiqjjrty', '2020-10-24 11:52:34.049060', NULL, NULL, NULL, 2),
(3, NULL, 1, 1, 'hYy5z7cekvzHg2vkTOKhVaEyWhXKuMBxxOBkx1WuZSNnMFoqtIyz2oB0fPHXU6HK', '2020-10-24 13:16:30.926087', NULL, NULL, NULL, 2),
(5, NULL, 1, 1, 'RxtZ7QS07hQBqClBakO4Z7xxXHAxyZZmmVmFjrSZrtQ3zjsuQPd5wnX0IkiZ3Nzl', '2021-05-11 10:41:08.480615', NULL, NULL, NULL, 2),
(7, NULL, 1, 1, 'Ffy2YtH0Vtp1vruJUrW2ClO9AhGarmP1FGLpoy4woEzpQ764uB7qTsMHYS7LT9SO', '2021-05-14 09:07:33.937660', NULL, NULL, NULL, 2),
(15, NULL, 1, 1, 'j2yDnDJfgdtn0kgX40Utgrtu06Q2edt9RcRNaUsZv0KAJ4AgnJIr70EUzTECaWYA', '2021-05-14 18:35:43.389921', NULL, NULL, NULL, 2),
(18, 2, 1, 2, '', '2021-05-15 09:58:19.711093', NULL, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sih_category`
--

CREATE TABLE `sih_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `cat_code` varchar(250) DEFAULT NULL,
  `cat_name` varchar(250) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime(6) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_category`
--

INSERT INTO `sih_category` (`id`, `parent_id`, `cat_code`, `cat_name`, `status`, `created_on`, `created_by`, `modified_on`, `modified_by`) VALUES
(1, 0, 'CAT0001', 'Sweet', 1, '2020-10-24 10:01:07.421350', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sih_category_codes`
--

CREATE TABLE `sih_category_codes` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_category_codes`
--

INSERT INTO `sih_category_codes` (`id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `sih_cms`
--

CREATE TABLE `sih_cms` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime(6) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sih_coupon`
--

CREATE TABLE `sih_coupon` (
  `id` int(11) NOT NULL,
  `coupon_code` varchar(250) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `customer_frequency` int(11) DEFAULT NULL,
  `offer_percentage` int(11) DEFAULT NULL,
  `min_order_amount` double DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime(6) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sih_coupon_codes`
--

CREATE TABLE `sih_coupon_codes` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sih_orders`
--

CREATE TABLE `sih_orders` (
  `id` int(11) NOT NULL,
  `order_code` varchar(250) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `shipping_cost` double DEFAULT NULL,
  `discount_cost` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `balance_unpaid` double DEFAULT NULL,
  `payment_type` int(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime(6) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `subscription` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_orders`
--

INSERT INTO `sih_orders` (`id`, `order_code`, `user_id`, `tax`, `shipping_cost`, `discount_cost`, `total`, `balance_unpaid`, `payment_type`, `status`, `created_on`, `created_by`, `modified_on`, `modified_by`, `subscription`) VALUES
(1, 'ORD0001', 2, 0, 0, 0, 95, 95, NULL, 1, '2021-05-11 11:09:33.551464', 2, NULL, NULL, NULL),
(2, 'ORD0002', 2, 0, 0, 0, 285, 285, NULL, 1, '2021-05-15 07:37:51.661856', 2, NULL, NULL, NULL),
(3, 'ORD0003', 2, 0, 0, 0, 95, 95, NULL, 1, '2021-05-15 07:55:11.256693', 2, NULL, NULL, NULL),
(4, 'ORD0005', 2, 0, 0, 0, 95, 95, NULL, 1, '2021-05-15 08:27:14.742130', 2, NULL, NULL, NULL),
(5, 'ORD0006', 2, 0, 0, 0, 95, 95, NULL, 1, '2021-05-15 08:28:10.262641', 2, NULL, NULL, NULL),
(6, 'ORD0007', 2, 0, 0, 0, 95, 95, NULL, 4, '2021-05-15 08:56:36.255147', 2, '2021-05-15 08:57:01.238591', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sih_order_bill_address`
--

CREATE TABLE `sih_order_bill_address` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `address1` varchar(250) DEFAULT NULL,
  `address2` varchar(250) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `postal_code` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `mobile` varchar(250) DEFAULT NULL,
  `land_mark` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_order_bill_address`
--

INSERT INTO `sih_order_bill_address` (`id`, `order_id`, `address1`, `address2`, `city`, `state`, `postal_code`, `country`, `email`, `mobile`, `land_mark`) VALUES
(1, 1, 'Star residency', 'north street', 'chennai', 'tamilnadu', '600012', 'india', '', '1234567890', 'railway station'),
(2, 2, 'Star residency', 'north street', 'chennai', 'tamilnadu', '600012', 'india', '', '1234567890', 'railway station'),
(3, 3, 'Star residency', 'north street', 'chennai', 'tamilnadu', '600012', 'india', '', '1234567890', 'railway station'),
(4, 6, 'Star residency', 'north street', 'chennai', 'tamilnadu', '600012', 'india', '', '1234567890', 'railway station');

-- --------------------------------------------------------

--
-- Table structure for table `sih_order_codes`
--

CREATE TABLE `sih_order_codes` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_order_codes`
--

INSERT INTO `sih_order_codes` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7);

-- --------------------------------------------------------

--
-- Table structure for table `sih_order_coupon`
--

CREATE TABLE `sih_order_coupon` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `coupon_titile` varchar(250) DEFAULT NULL,
  `discount_percentage` int(11) DEFAULT NULL,
  `actual_amount` double DEFAULT NULL,
  `discount_amount` double DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sih_order_log`
--

CREATE TABLE `sih_order_log` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `field` varchar(250) DEFAULT NULL,
  `old_value` varchar(250) DEFAULT NULL,
  `new_value` varchar(250) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_order_log`
--

INSERT INTO `sih_order_log` (`id`, `order_id`, `field`, `old_value`, `new_value`, `description`, `created_on`, `created_by`) VALUES
(1, 6, 'status', 'new', 'cancelled', 'status changed from new to cancelled', '2021-05-15 08:57:01.237658', 2);

-- --------------------------------------------------------

--
-- Table structure for table `sih_order_products`
--

CREATE TABLE `sih_order_products` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `prd_id` int(11) DEFAULT NULL,
  `prd_name` varchar(250) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `prd_qty` int(11) DEFAULT NULL,
  `prd_weight` int(11) DEFAULT NULL,
  `prd_price` double DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime(6) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `prd_subscription` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_order_products`
--

INSERT INTO `sih_order_products` (`id`, `order_id`, `prd_id`, `prd_name`, `package_id`, `prd_qty`, `prd_weight`, `prd_price`, `created_on`, `created_by`, `modified_on`, `modified_by`, `prd_subscription`) VALUES
(1, 1, 1, 'Laddu', 1, 1, 100, 95, '2021-05-11 11:09:33.625291', 2, NULL, NULL, NULL),
(2, 2, 1, 'Laddu', 1, 1, 100, 95, '2021-05-15 07:37:51.703273', 2, NULL, NULL, NULL),
(3, 3, 1, 'Laddu', 1, 1, 100, 95, '2021-05-15 07:55:11.300011', 2, NULL, NULL, 1),
(4, 6, 1, 'Laddu', 1, 1, 100, 95, '2021-05-15 08:56:36.299253', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sih_order_shipping`
--

CREATE TABLE `sih_order_shipping` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `shipped_date` date DEFAULT NULL,
  `in_hand_date` date DEFAULT NULL,
  `shipping_method_id` int(11) DEFAULT NULL,
  `tracking_numbers` longtext DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime(6) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sih_order_ship_address`
--

CREATE TABLE `sih_order_ship_address` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `address1` varchar(250) DEFAULT NULL,
  `address2` varchar(250) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `postal_code` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `mobile` varchar(250) DEFAULT NULL,
  `land_mark` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_order_ship_address`
--

INSERT INTO `sih_order_ship_address` (`id`, `order_id`, `address1`, `address2`, `city`, `state`, `postal_code`, `country`, `email`, `mobile`, `land_mark`) VALUES
(1, 1, 'Star residency', 'north street', 'chennai', 'tamilnadu', '600012', 'india', '', '1234567890', 'railway station'),
(2, 2, 'Star residency', 'north street', 'chennai', 'tamilnadu', '600012', 'india', '', '1234567890', 'railway station'),
(3, 3, 'Star residency', 'north street', 'chennai', 'tamilnadu', '600012', 'india', '', '1234567890', 'railway station'),
(4, 6, 'Star residency', 'north street', 'chennai', 'tamilnadu', '600012', 'india', '', '1234567890', 'railway station');

-- --------------------------------------------------------

--
-- Table structure for table `sih_order_status`
--

CREATE TABLE `sih_order_status` (
  `id` int(11) NOT NULL,
  `status_id` int(11) DEFAULT NULL,
  `status_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sih_prd_package_type`
--

CREATE TABLE `sih_prd_package_type` (
  `id` int(11) NOT NULL,
  `code` varchar(250) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime(6) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_prd_package_type`
--

INSERT INTO `sih_prd_package_type` (`id`, `code`, `name`, `status`, `created_on`, `created_by`, `modified_on`, `modified_by`) VALUES
(1, 'BOTTLE', 'Bottle', 1, NULL, NULL, NULL, NULL),
(2, 'PLASTIC BAG', 'Plastic Bag', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sih_product`
--

CREATE TABLE `sih_product` (
  `id` int(11) NOT NULL,
  `product_code` varchar(250) DEFAULT NULL,
  `product_group_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `pack_id` int(11) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `seo_name` varchar(250) DEFAULT NULL,
  `mat_code` varchar(250) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `offer_price` double DEFAULT NULL,
  `actual_price` double DEFAULT NULL,
  `gtin` int(11) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime(6) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_product`
--

INSERT INTO `sih_product` (`id`, `product_code`, `product_group_id`, `category_id`, `pack_id`, `name`, `seo_name`, `mat_code`, `weight`, `offer_price`, `actual_price`, `gtin`, `description`, `status`, `created_on`, `created_by`, `modified_on`, `modified_by`) VALUES
(1, 'PRT0001', 1, 1, 1, 'Laddu', 'laddu', 'mat123', 100, 95, 100, 123232, 'Hand made sweet .this is specially for kids ,they are really like this item\r\n', 1, '2020-10-24 10:03:08.673735', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sih_product_codes`
--

CREATE TABLE `sih_product_codes` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_product_codes`
--

INSERT INTO `sih_product_codes` (`id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `sih_product_group`
--

CREATE TABLE `sih_product_group` (
  `id` int(11) NOT NULL,
  `prd_group_code` varchar(250) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime(6) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_product_group`
--

INSERT INTO `sih_product_group` (`id`, `prd_group_code`, `category_id`, `name`, `description`, `status`, `created_on`, `created_by`, `modified_on`, `modified_by`) VALUES
(1, 'PRTGRP0001', 1, 'Hand made', NULL, 1, '2020-10-24 10:01:53.556521', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sih_product_group_codes`
--

CREATE TABLE `sih_product_group_codes` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_product_group_codes`
--

INSERT INTO `sih_product_group_codes` (`id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `sih_product_group_image`
--

CREATE TABLE `sih_product_group_image` (
  `id` int(11) NOT NULL,
  `prd_group_id` int(11) DEFAULT NULL,
  `image` longtext DEFAULT NULL,
  `is_default` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sih_product_image`
--

CREATE TABLE `sih_product_image` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `image` longtext DEFAULT NULL,
  `is_default` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sih_shipping_method`
--

CREATE TABLE `sih_shipping_method` (
  `id` int(11) NOT NULL,
  `carrier` varchar(100) DEFAULT NULL,
  `method` varchar(100) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime(6) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sih_ship_weight_cost`
--

CREATE TABLE `sih_ship_weight_cost` (
  `id` int(11) NOT NULL,
  `min_weight` double DEFAULT NULL,
  `max_weight` double DEFAULT NULL,
  `exc_vat` double DEFAULT NULL,
  `inc_vat` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sih_subscription`
--

CREATE TABLE `sih_subscription` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `subscription_month` int(11) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime(6) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_subscription`
--

INSERT INTO `sih_subscription` (`id`, `product_id`, `subscription_month`, `created_on`, `created_by`, `modified_on`, `modified_by`) VALUES
(1, 1, 2, '2021-05-14 18:04:10.615115', NULL, '2021-05-15 09:59:05.354627', NULL),
(2, 1, 2, '2021-05-14 18:35:43.424606', NULL, '2021-05-15 09:59:05.354627', NULL),
(3, 1, 2, '2021-05-15 07:38:20.628482', NULL, '2021-05-15 09:59:05.354627', NULL),
(4, 1, 2, '2021-05-15 08:13:49.972728', NULL, '2021-05-15 09:59:05.354627', NULL),
(5, 1, 2, '2021-05-15 09:58:19.781664', NULL, '2021-05-15 09:59:05.354627', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sih_users`
--

CREATE TABLE `sih_users` (
  `id` int(11) NOT NULL,
  `user_code` varchar(250) DEFAULT NULL,
  `user_type_id` int(11) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime(6) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_users`
--

INSERT INTO `sih_users` (`id`, `user_code`, `user_type_id`, `email`, `password`, `status`, `created_on`, `created_by`, `modified_on`, `modified_by`) VALUES
(1, 'USR0001', 1, 'admin@mailinator.com', 'admin', 1, '2020-10-22 20:28:53.000000', 1, NULL, NULL),
(2, 'USR0002', 2, 'vignesh@mailinator.com', 'vignesh', 1, '2020-10-23 20:29:00.000000', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sih_user_codes`
--

CREATE TABLE `sih_user_codes` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_user_codes`
--

INSERT INTO `sih_user_codes` (`id`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Table structure for table `sih_user_profile`
--

CREATE TABLE `sih_user_profile` (
  `id` int(11) NOT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `gender` smallint(6) DEFAULT NULL,
  `contact_no` varchar(250) DEFAULT NULL,
  `image` longtext DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime(6) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_user_profile`
--

INSERT INTO `sih_user_profile` (`id`, `first_name`, `last_name`, `user_id`, `dob`, `gender`, `contact_no`, `image`, `created_on`, `created_by`, `modified_on`, `modified_by`) VALUES
(1, 'Admin', 'user', 1, '1995-06-17', 1, NULL, NULL, '2020-10-22 20:28:23.000000', 1, NULL, NULL),
(2, 'Vignesh', 'R', 2, '1995-10-20', 1, '8883023140', NULL, '2020-10-23 20:28:38.000000', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sih_user_type`
--

CREATE TABLE `sih_user_type` (
  `id` int(11) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime(6) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sih_user_type`
--

INSERT INTO `sih_user_type` (`id`, `code`, `name`, `status`, `created_on`, `created_by`, `modified_on`, `modified_by`) VALUES
(1, 'EMPLOYEE', 'Employee', 1, NULL, NULL, NULL, NULL),
(2, 'CUSTOMER', 'Customer', 1, NULL, NULL, NULL, NULL),
(3, 'DEALER', 'Dealer', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sih_weights`
--

CREATE TABLE `sih_weights` (
  `id` int(11) NOT NULL,
  `measurement` varchar(100) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `created_on` datetime(6) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime(6) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `sih_address`
--
ALTER TABLE `sih_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_cart`
--
ALTER TABLE `sih_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_category`
--
ALTER TABLE `sih_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_category_codes`
--
ALTER TABLE `sih_category_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_cms`
--
ALTER TABLE `sih_cms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_coupon`
--
ALTER TABLE `sih_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_coupon_codes`
--
ALTER TABLE `sih_coupon_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_orders`
--
ALTER TABLE `sih_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_order_bill_address`
--
ALTER TABLE `sih_order_bill_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_order_codes`
--
ALTER TABLE `sih_order_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_order_coupon`
--
ALTER TABLE `sih_order_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_order_log`
--
ALTER TABLE `sih_order_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_order_products`
--
ALTER TABLE `sih_order_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_order_shipping`
--
ALTER TABLE `sih_order_shipping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_order_ship_address`
--
ALTER TABLE `sih_order_ship_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_order_status`
--
ALTER TABLE `sih_order_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_prd_package_type`
--
ALTER TABLE `sih_prd_package_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_product`
--
ALTER TABLE `sih_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_product_codes`
--
ALTER TABLE `sih_product_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_product_group`
--
ALTER TABLE `sih_product_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_product_group_codes`
--
ALTER TABLE `sih_product_group_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_product_group_image`
--
ALTER TABLE `sih_product_group_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_product_image`
--
ALTER TABLE `sih_product_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_shipping_method`
--
ALTER TABLE `sih_shipping_method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_ship_weight_cost`
--
ALTER TABLE `sih_ship_weight_cost`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_subscription`
--
ALTER TABLE `sih_subscription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_users`
--
ALTER TABLE `sih_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_user_codes`
--
ALTER TABLE `sih_user_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_user_profile`
--
ALTER TABLE `sih_user_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_user_type`
--
ALTER TABLE `sih_user_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sih_weights`
--
ALTER TABLE `sih_weights`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `sih_address`
--
ALTER TABLE `sih_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sih_cart`
--
ALTER TABLE `sih_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `sih_category`
--
ALTER TABLE `sih_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sih_category_codes`
--
ALTER TABLE `sih_category_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sih_cms`
--
ALTER TABLE `sih_cms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sih_coupon`
--
ALTER TABLE `sih_coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sih_coupon_codes`
--
ALTER TABLE `sih_coupon_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sih_orders`
--
ALTER TABLE `sih_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sih_order_bill_address`
--
ALTER TABLE `sih_order_bill_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sih_order_codes`
--
ALTER TABLE `sih_order_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sih_order_coupon`
--
ALTER TABLE `sih_order_coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sih_order_log`
--
ALTER TABLE `sih_order_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sih_order_products`
--
ALTER TABLE `sih_order_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sih_order_shipping`
--
ALTER TABLE `sih_order_shipping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sih_order_ship_address`
--
ALTER TABLE `sih_order_ship_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sih_order_status`
--
ALTER TABLE `sih_order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sih_prd_package_type`
--
ALTER TABLE `sih_prd_package_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sih_product`
--
ALTER TABLE `sih_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sih_product_codes`
--
ALTER TABLE `sih_product_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sih_product_group`
--
ALTER TABLE `sih_product_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sih_product_group_codes`
--
ALTER TABLE `sih_product_group_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sih_product_group_image`
--
ALTER TABLE `sih_product_group_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sih_product_image`
--
ALTER TABLE `sih_product_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sih_shipping_method`
--
ALTER TABLE `sih_shipping_method`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sih_ship_weight_cost`
--
ALTER TABLE `sih_ship_weight_cost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sih_subscription`
--
ALTER TABLE `sih_subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sih_users`
--
ALTER TABLE `sih_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sih_user_codes`
--
ALTER TABLE `sih_user_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sih_user_profile`
--
ALTER TABLE `sih_user_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sih_user_type`
--
ALTER TABLE `sih_user_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sih_weights`
--
ALTER TABLE `sih_weights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
